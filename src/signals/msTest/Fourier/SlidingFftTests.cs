using System;
using System.Diagnostics;
using System.Numerics;

using isr.Signals.Base;
using isr.Signals.Fourier;
using isr.Signals.MetaNumerics;
using isr.Signals.Waveform;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Signals.MSTest
{

    /// <summary>   (Unit Test Class) a sliding FFT tests. </summary>
    /// <remarks>   David, 2022-02-19. </remarks>
    [TestClass()]
    public class SlidingFftTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:u}" );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " Sliding FFT Tests "

        private static double[] ComputeSlidingFft( int fftPoints, double noiseFigure, int steps )
        {
            Console.Out.WriteLine( "Calculating double-precision sliding FFT" );

            Random rnd = new Random( fftPoints );
            double phase = 0;
            double frequency = 2;

            // ------------ Create the Signal for FFT ---------------

            // Create cycles of the sine wave.
            // get the signal
            var signal = Signal.Sine( frequency, phase, fftPoints );

            // Allocate the spectrum array.
            var fftValues = new System.Numerics.Complex[fftPoints];
            signal.CopyTo( fftValues );

            // ------------ Calculate Forward and Inverse FFT ---------------

            var duration = TimeSpan.Zero;
            // Create a new instance of the Mixed Radix class
            using ( var fft = new MixedRadixFourierTransform() )
            {
                // Compute the FFT.
                Stopwatch timeKeeper;
                timeKeeper = new Stopwatch();
                timeKeeper.Restart();
                fft.Forward( fftValues );
                duration = timeKeeper.Elapsed;
            }

            // Display time
            Console.Out.WriteLine( $"{duration.TotalMilliseconds:0.###} ms" );

            // Gets the FFT magnitudes
            double[] magnitudes = Array.Empty<double>();

            // Initialize the last point of the signal to the last point.
            int lastSignalPoint = fftPoints - 1;

            // Initialize the first point of the signal to the last point.
            int firstSignalPoint = 0;

            // Calculate the phase of the last point of the sine wave
            double deltaPhase = 2d * Math.PI * frequency / Convert.ToSingle( fftPoints );

            // get the signal phase of the last point
            double signalPhase = Signal.ToRadians( phase );
            signalPhase += deltaPhase * lastSignalPoint;

            // Use a new frequency for the sine wave to see how
            // the old is phased out and the new gets in
            deltaPhase *= 2d;

            // First element in the previous value of the time series
            Complex oldValue;

            // New value from the Signal
            Complex newValue;

            // Create a new instance of the sliding FFT class
            using var slidingFft = new SlidingFourierTransform();

            // Initialize sliding FFT coefficients.
            slidingFft.Forward( fftValues );

            // Recalculate the sliding FFT for half cycle of points
            while ( steps > 0 )
            {

                // ------- Calculate FFT outcomes ---------

                // Get the FFT magnitudes
                magnitudes = slidingFft.Dft().Magnitudes();

                // ------------ update the sliding fft ---------------

                // Update the previous values of the signal
                oldValue = new Complex( signal[firstSignalPoint], 0d );

                // Add some random noise to make it interesting.
                double noise = noiseFigure * 2d * (rnd.NextDouble() - 0.5d);

                // Get new signal values.
                signalPhase += deltaPhase;
                newValue = new Complex( Math.Sin( signalPhase ) + noise, 0d );

                // Update the signal itself.

                // Update the last point.
                lastSignalPoint += 1;
                if ( lastSignalPoint >= fftPoints )
                {
                    lastSignalPoint = 0;
                }

                // Update the first point.
                firstSignalPoint += 1;
                if ( firstSignalPoint >= fftPoints )
                {
                    firstSignalPoint = 0;
                }

                // Place new data in the signal itself.
                signal[lastSignalPoint] = newValue.Real;

                // Calculate the sliding FFT coefficients.
                slidingFft.Update( newValue, oldValue );

                steps--;

            }
            return magnitudes;
        }

        /// <summary> (Unit Test Method) tests double precision discrete Fourier transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        [TestMethod()]
        public void SlidingFftShouldCompute()
        {
            int fftPoints = 32;
            double noiseFigure = 0.5;
            int steps = 1;
            double[] expected = new double[] { 0, 0, 16 };
            double epsilon = 0.001;
            double[] actual = SlidingFftTests.ComputeSlidingFft( fftPoints, noiseFigure, steps );
            for ( int i = 0; i < expected.Length; i++ )
                Assert.AreEqual( expected[i], actual[i] , epsilon, $"at {i}");

            steps = 2;
            expected = new double[] { 0.326, 0.326, 16.003 };
            actual = SlidingFftTests.ComputeSlidingFft( fftPoints, noiseFigure, steps );
            for ( int i = 0; i < expected.Length; i++ )
                Assert.AreEqual( expected[i], actual[i], epsilon, $"at {i}" );

            steps = 3;
            expected = new double[] { 0.574, 0.571, 16.104 };
            actual = SlidingFftTests.ComputeSlidingFft( fftPoints, noiseFigure, steps );
            for ( int i = 0; i < expected.Length; i++ )
                Assert.AreEqual( expected[i], actual[i], epsilon, $"at {i}" );

            steps = 4;
            expected = new double[] { 0.426, 0.433, 15.996 };
            actual = SlidingFftTests.ComputeSlidingFft( fftPoints, noiseFigure, steps );
            for ( int i = 0; i < expected.Length; i++ )
                Assert.AreEqual( expected[i], actual[i], epsilon, $"at {i}" );

        }

        #endregion

    }
}
