using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.Signals.MSTest.Wisdom
{

    /// <summary> Wisdom unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    public class WisdomTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:u}" );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #endregion

        #region " SPECTRUM TESTS "

        /// <summary>   Tests double precision spectrum. </summary>
        /// <remarks>   David, 2020-10-26. </remarks>
        /// <param name="fft">          The FFT. </param>
        /// <param name="scaleFft">     True to scale FFT. </param>
        /// <param name="removeMean">   True to remove mean. </param>
        private static void DoublePrecisionSpectrumTest(isr.Signals.Base.FourierTransformBase fft, bool scaleFft, bool removeMean)
        {
            // scale the FFT by the Window power and data points
            // Remove mean before calculating the FFT
            using var spectrum = new isr.Signals.Spectrum.Spectrum( fft ) { IsScaleFft = scaleFft, IsRemoveMean = removeMean };
            spectrum.IsScaleFft = scaleFft;
            spectrum.IsRemoveMean = removeMean;
            Assert.AreEqual( scaleFft, spectrum.IsScaleFft, $"{nameof( isr.Signals.Spectrum.Spectrum.IsScaleFft )} should be {scaleFft}" );
        }

        /// <summary> (Unit Test Method) tests double precision discrete Fourier transform. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        [TestMethod()]
        public void DoublePrecisionDiscreteFourierTransformTest()
        {
            bool scaleFft = true;
            bool removemean = true;
            using isr.Signals.Base.FourierTransformBase fft = new isr.Signals.Wisdom.Dft();
            DoublePrecisionSpectrumTest( fft, scaleFft, removemean );
        }

        #endregion

    }
}
