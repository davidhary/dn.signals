using System;
using System.Windows.Forms;

namespace isr.Signals.Demo
{
    public partial class Console : Form
    {
        public Console()
        {
            this.InitializeComponent();
        }

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing )
            {
                this.components?.Dispose();
                this._SpectrumView?.Dispose();
            }
            base.Dispose( disposing );
        }

        private UserControl _SpectrumView;
        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Name} adding spectrum view";
                switch ( CommandLineInfo.UserInterface )
                {
                    case UserInterface.Spectrum:
                        {
                            this._SpectrumView = new SpectrumView();
                            break;
                        }

                    case UserInterface.Wisdom:
                        {
                            this._SpectrumView = new WisdomSpectrumView();
                            break;
                        }

                    default:
                        {
                            this._SpectrumView = new SpectrumView();
                            break;
                        }
                }

                this.Controls.Add( this._SpectrumView );
                this._SpectrumView.Dock = DockStyle.Fill;
                this.Text = CommandLineInfo.UserInterface.ToString();
            }
            catch ( Exception ex )
            {
                System.Diagnostics.Trace.WriteLine( $"Exception {activity}; Details: {ex}" );
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnShown( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if ( this._SpectrumView is object )
                    this._SpectrumView.Cursor = Cursors.WaitCursor;
                activity = $"{this.Name} showing dialog";
                base.OnShown( e );
            }
            catch ( Exception ex )
            {
                System.Diagnostics.Trace.WriteLine( $"Exception {activity}; Details: {ex}" );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if ( this._SpectrumView is object )
                    this._SpectrumView.Cursor = Cursors.Default;
            }
        }


    }
}
