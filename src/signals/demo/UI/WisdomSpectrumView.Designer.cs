using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Signals.Demo
{
    [DesignerGenerated()]
    public partial class WisdomSpectrumView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._PointsToDisplayTextBox = new System.Windows.Forms.TextBox();
            this._FftListBox = new System.Windows.Forms.ListBox();
            this._DataTabPage = new System.Windows.Forms.TabPage();
            this._TimingTextBox = new System.Windows.Forms.TextBox();
            this._TimingListBoxLabel = new System.Windows.Forms.Label();
            this._PointsToDisplayTextBoxLabel = new System.Windows.Forms.Label();
            this._FftListBoxLabel = new System.Windows.Forms.Label();
            this._DoubleRadioButton = new System.Windows.Forms.RadioButton();
            this._RemoveMeanCheckBox = new System.Windows.Forms.CheckBox();
            this._ExampleComboBox = new System.Windows.Forms.ComboBox();
            this._TaperWindowCheckBox = new System.Windows.Forms.CheckBox();
            this._FftTimeStatusBarPanel = new System.Windows.Forms.ToolStripStatusLabel();
            this._ErrorStatusBarPanel = new System.Windows.Forms.ToolStripStatusLabel();
            this._MainTabControl = new System.Windows.Forms.TabControl();
            this._SignalTabPage = new System.Windows.Forms.TabPage();
            this._SignalChartPanel = new System.Windows.Forms.Panel();
            this._SignalOptionsPanel = new System.Windows.Forms.Panel();
            this._SignalDurationTextBox = new System.Windows.Forms.TextBox();
            this._SignalDurationTextBoxLabel = new System.Windows.Forms.Label();
            this._PointsTextBox = new System.Windows.Forms.TextBox();
            this._PhaseTextBox = new System.Windows.Forms.TextBox();
            this._CyclesTextBox = new System.Windows.Forms.TextBox();
            this._PointsTextBoxLabel = new System.Windows.Forms.Label();
            this._PhaseTextBoxLabel = new System.Windows.Forms.Label();
            this._CyclesTextBoxLabel = new System.Windows.Forms.Label();
            this._SpectrumTabPage = new System.Windows.Forms.TabPage();
            this._SpectrumChartPanel = new System.Windows.Forms.Panel();
            this._SpectrumOptionsPanel = new System.Windows.Forms.Panel();
            this._ExampleComboBoxLabel = new System.Windows.Forms.Label();
            this._StartStopCheckBox = new System.Windows.Forms.CheckBox();
            this._SingleRadioButton = new System.Windows.Forms.RadioButton();
            this._MessagesTabPage = new System.Windows.Forms.TabPage();
            this._NotesMessageList = new System.Windows.Forms.TextBox();
            this._ErrorErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this._CountStatusBarPanel = new System.Windows.Forms.ToolStripStatusLabel();
            this._StatusStatusBarPanel = new System.Windows.Forms.ToolStripStatusLabel();
            this._MainStatusBar = new System.Windows.Forms.StatusStrip();
            this._DataTabPage.SuspendLayout();
            this._MainTabControl.SuspendLayout();
            this._SignalTabPage.SuspendLayout();
            this._SignalOptionsPanel.SuspendLayout();
            this._SpectrumTabPage.SuspendLayout();
            this._SpectrumOptionsPanel.SuspendLayout();
            this._MessagesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorErrorProvider)).BeginInit();
            this._MainStatusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // _PointsToDisplayTextBox
            // 
            this._PointsToDisplayTextBox.AcceptsReturn = true;
            this._PointsToDisplayTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._PointsToDisplayTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._PointsToDisplayTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._PointsToDisplayTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._PointsToDisplayTextBox.Location = new System.Drawing.Point(149, 18);
            this._PointsToDisplayTextBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._PointsToDisplayTextBox.MaxLength = 0;
            this._PointsToDisplayTextBox.Name = "_PointsToDisplayTextBox";
            this._PointsToDisplayTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PointsToDisplayTextBox.Size = new System.Drawing.Size(75, 20);
            this._PointsToDisplayTextBox.TabIndex = 17;
            this._PointsToDisplayTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.PointsToDisplayTextBox_Validating);
            // 
            // _FftListBox
            // 
            this._FftListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._FftListBox.BackColor = System.Drawing.SystemColors.Window;
            this._FftListBox.Cursor = System.Windows.Forms.Cursors.Default;
            this._FftListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._FftListBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._FftListBox.Location = new System.Drawing.Point(317, 28);
            this._FftListBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._FftListBox.Name = "_FftListBox";
            this._FftListBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._FftListBox.Size = new System.Drawing.Size(313, 342);
            this._FftListBox.TabIndex = 15;
            // 
            // _DataTabPage
            // 
            this._DataTabPage.Controls.Add(this._TimingTextBox);
            this._DataTabPage.Controls.Add(this._TimingListBoxLabel);
            this._DataTabPage.Controls.Add(this._PointsToDisplayTextBox);
            this._DataTabPage.Controls.Add(this._FftListBox);
            this._DataTabPage.Controls.Add(this._PointsToDisplayTextBoxLabel);
            this._DataTabPage.Controls.Add(this._FftListBoxLabel);
            this._DataTabPage.Location = new System.Drawing.Point(4, 24);
            this._DataTabPage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._DataTabPage.Name = "_DataTabPage";
            this._DataTabPage.Size = new System.Drawing.Size(645, 397);
            this._DataTabPage.TabIndex = 2;
            this._DataTabPage.Text = "Data";
            // 
            // _TimingTextBox
            // 
            this._TimingTextBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._TimingTextBox.Location = new System.Drawing.Point(23, 81);
            this._TimingTextBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._TimingTextBox.Multiline = true;
            this._TimingTextBox.Name = "_TimingTextBox";
            this._TimingTextBox.Size = new System.Drawing.Size(286, 296);
            this._TimingTextBox.TabIndex = 21;
            // 
            // _TimingListBoxLabel
            // 
            this._TimingListBoxLabel.AutoSize = true;
            this._TimingListBoxLabel.Location = new System.Drawing.Point(19, 62);
            this._TimingListBoxLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._TimingListBoxLabel.Name = "_TimingListBoxLabel";
            this._TimingListBoxLabel.Size = new System.Drawing.Size(77, 15);
            this._TimingListBoxLabel.TabIndex = 20;
            this._TimingListBoxLabel.Text = "Timing Data: ";
            // 
            // _PointsToDisplayTextBoxLabel
            // 
            this._PointsToDisplayTextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._PointsToDisplayTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._PointsToDisplayTextBoxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._PointsToDisplayTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._PointsToDisplayTextBoxLabel.Location = new System.Drawing.Point(19, 23);
            this._PointsToDisplayTextBoxLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._PointsToDisplayTextBoxLabel.Name = "_PointsToDisplayTextBoxLabel";
            this._PointsToDisplayTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PointsToDisplayTextBoxLabel.Size = new System.Drawing.Size(121, 18);
            this._PointsToDisplayTextBoxLabel.TabIndex = 18;
            this._PointsToDisplayTextBoxLabel.Text = "Points to Display: ";
            this._PointsToDisplayTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _FftListBoxLabel
            // 
            this._FftListBoxLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._FftListBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._FftListBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._FftListBoxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._FftListBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._FftListBoxLabel.Location = new System.Drawing.Point(317, 9);
            this._FftListBoxLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._FftListBoxLabel.Name = "_FftListBoxLabel";
            this._FftListBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._FftListBoxLabel.Size = new System.Drawing.Size(314, 18);
            this._FftListBoxLabel.TabIndex = 16;
            this._FftListBoxLabel.Text = "Signal                       IFFT{ FFT{ Signal}}";
            // 
            // _DoubleRadioButton
            // 
            this._DoubleRadioButton.Checked = true;
            this._DoubleRadioButton.Location = new System.Drawing.Point(131, 6);
            this._DoubleRadioButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._DoubleRadioButton.Name = "_DoubleRadioButton";
            this._DoubleRadioButton.Size = new System.Drawing.Size(140, 20);
            this._DoubleRadioButton.TabIndex = 27;
            this._DoubleRadioButton.TabStop = true;
            this._DoubleRadioButton.Text = "Double Precision";
            // 
            // _RemoveMeanCheckBox
            // 
            this._RemoveMeanCheckBox.BackColor = System.Drawing.SystemColors.Control;
            this._RemoveMeanCheckBox.Cursor = System.Windows.Forms.Cursors.Default;
            this._RemoveMeanCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._RemoveMeanCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this._RemoveMeanCheckBox.Location = new System.Drawing.Point(9, 5);
            this._RemoveMeanCheckBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._RemoveMeanCheckBox.Name = "_RemoveMeanCheckBox";
            this._RemoveMeanCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._RemoveMeanCheckBox.Size = new System.Drawing.Size(121, 21);
            this._RemoveMeanCheckBox.TabIndex = 23;
            this._RemoveMeanCheckBox.Text = "Remove Mean";
            this._RemoveMeanCheckBox.UseVisualStyleBackColor = false;
            // 
            // _ExampleComboBox
            // 
            this._ExampleComboBox.BackColor = System.Drawing.SystemColors.Window;
            this._ExampleComboBox.Cursor = System.Windows.Forms.Cursors.Default;
            this._ExampleComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._ExampleComboBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._ExampleComboBox.Items.AddRange(new object[] {
            "Mixed Radix FFT",
            "Sliding FFT"});
            this._ExampleComboBox.Location = new System.Drawing.Point(355, 31);
            this._ExampleComboBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._ExampleComboBox.Name = "_ExampleComboBox";
            this._ExampleComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._ExampleComboBox.Size = new System.Drawing.Size(195, 21);
            this._ExampleComboBox.TabIndex = 25;
            this._ExampleComboBox.Text = "Mixed Radix FFT";
            // 
            // _TaperWindowCheckBox
            // 
            this._TaperWindowCheckBox.BackColor = System.Drawing.SystemColors.Control;
            this._TaperWindowCheckBox.Cursor = System.Windows.Forms.Cursors.Default;
            this._TaperWindowCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._TaperWindowCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this._TaperWindowCheckBox.Location = new System.Drawing.Point(9, 28);
            this._TaperWindowCheckBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._TaperWindowCheckBox.Name = "_TaperWindowCheckBox";
            this._TaperWindowCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._TaperWindowCheckBox.Size = new System.Drawing.Size(121, 21);
            this._TaperWindowCheckBox.TabIndex = 24;
            this._TaperWindowCheckBox.Text = "Taper Window";
            this._TaperWindowCheckBox.UseVisualStyleBackColor = false;
            // 
            // _FftTimeStatusBarPanel
            // 
            this._FftTimeStatusBarPanel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._FftTimeStatusBarPanel.Name = "_FftTimeStatusBarPanel";
            this._FftTimeStatusBarPanel.Size = new System.Drawing.Size(53, 17);
            this._FftTimeStatusBarPanel.Text = "0.000 ms";
            this._FftTimeStatusBarPanel.ToolTipText = "Time to calculate an FFT";
            // 
            // _ErrorStatusBarPanel
            // 
            this._ErrorStatusBarPanel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._ErrorStatusBarPanel.Name = "_ErrorStatusBarPanel";
            this._ErrorStatusBarPanel.Size = new System.Drawing.Size(34, 17);
            this._ErrorStatusBarPanel.Text = "0.000";
            this._ErrorStatusBarPanel.ToolTipText = "RMS difference between signal and inverse FFT";
            // 
            // _MainTabControl
            // 
            this._MainTabControl.Controls.Add(this._SignalTabPage);
            this._MainTabControl.Controls.Add(this._SpectrumTabPage);
            this._MainTabControl.Controls.Add(this._DataTabPage);
            this._MainTabControl.Controls.Add(this._MessagesTabPage);
            this._MainTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._MainTabControl.Location = new System.Drawing.Point(0, 0);
            this._MainTabControl.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._MainTabControl.Name = "_MainTabControl";
            this._MainTabControl.SelectedIndex = 0;
            this._MainTabControl.Size = new System.Drawing.Size(653, 428);
            this._MainTabControl.TabIndex = 2;
            // 
            // _SignalTabPage
            // 
            this._SignalTabPage.Controls.Add(this._SignalChartPanel);
            this._SignalTabPage.Controls.Add(this._SignalOptionsPanel);
            this._SignalTabPage.Location = new System.Drawing.Point(4, 24);
            this._SignalTabPage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._SignalTabPage.Name = "_SignalTabPage";
            this._SignalTabPage.Size = new System.Drawing.Size(645, 400);
            this._SignalTabPage.TabIndex = 0;
            this._SignalTabPage.Text = "Signal";
            // 
            // _SignalChartPanel
            // 
            this._SignalChartPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._SignalChartPanel.Location = new System.Drawing.Point(0, 65);
            this._SignalChartPanel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._SignalChartPanel.Name = "_SignalChartPanel";
            this._SignalChartPanel.Size = new System.Drawing.Size(645, 335);
            this._SignalChartPanel.TabIndex = 30;
            // 
            // _SignalOptionsPanel
            // 
            this._SignalOptionsPanel.Controls.Add(this._SignalDurationTextBox);
            this._SignalOptionsPanel.Controls.Add(this._SignalDurationTextBoxLabel);
            this._SignalOptionsPanel.Controls.Add(this._PointsTextBox);
            this._SignalOptionsPanel.Controls.Add(this._PhaseTextBox);
            this._SignalOptionsPanel.Controls.Add(this._CyclesTextBox);
            this._SignalOptionsPanel.Controls.Add(this._PointsTextBoxLabel);
            this._SignalOptionsPanel.Controls.Add(this._PhaseTextBoxLabel);
            this._SignalOptionsPanel.Controls.Add(this._CyclesTextBoxLabel);
            this._SignalOptionsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._SignalOptionsPanel.Location = new System.Drawing.Point(0, 0);
            this._SignalOptionsPanel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._SignalOptionsPanel.Name = "_SignalOptionsPanel";
            this._SignalOptionsPanel.Size = new System.Drawing.Size(645, 65);
            this._SignalOptionsPanel.TabIndex = 29;
            // 
            // _SignalDurationTextBox
            // 
            this._SignalDurationTextBox.AcceptsReturn = true;
            this._SignalDurationTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._SignalDurationTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._SignalDurationTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._SignalDurationTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._SignalDurationTextBox.Location = new System.Drawing.Point(105, 35);
            this._SignalDurationTextBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._SignalDurationTextBox.MaxLength = 0;
            this._SignalDurationTextBox.Name = "_SignalDurationTextBox";
            this._SignalDurationTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._SignalDurationTextBox.Size = new System.Drawing.Size(53, 20);
            this._SignalDurationTextBox.TabIndex = 23;
            // 
            // _SignalDurationTextBoxLabel
            // 
            this._SignalDurationTextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._SignalDurationTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._SignalDurationTextBoxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._SignalDurationTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._SignalDurationTextBoxLabel.Location = new System.Drawing.Point(9, 37);
            this._SignalDurationTextBoxLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._SignalDurationTextBoxLabel.Name = "_SignalDurationTextBoxLabel";
            this._SignalDurationTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._SignalDurationTextBoxLabel.Size = new System.Drawing.Size(93, 18);
            this._SignalDurationTextBoxLabel.TabIndex = 24;
            this._SignalDurationTextBoxLabel.Text = "Duration [Sec]: ";
            this._SignalDurationTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PointsTextBox
            // 
            this._PointsTextBox.AcceptsReturn = true;
            this._PointsTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._PointsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._PointsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._PointsTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._PointsTextBox.Location = new System.Drawing.Point(105, 5);
            this._PointsTextBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._PointsTextBox.MaxLength = 0;
            this._PointsTextBox.Name = "_PointsTextBox";
            this._PointsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PointsTextBox.Size = new System.Drawing.Size(53, 20);
            this._PointsTextBox.TabIndex = 22;
            this._PointsTextBox.Text = "1000";
            this._PointsTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.PointsTextBox_Validating);
            // 
            // _PhaseTextBox
            // 
            this._PhaseTextBox.AcceptsReturn = true;
            this._PhaseTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._PhaseTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._PhaseTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._PhaseTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._PhaseTextBox.Location = new System.Drawing.Point(280, 35);
            this._PhaseTextBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._PhaseTextBox.MaxLength = 0;
            this._PhaseTextBox.Name = "_PhaseTextBox";
            this._PhaseTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PhaseTextBox.Size = new System.Drawing.Size(55, 20);
            this._PhaseTextBox.TabIndex = 19;
            this._PhaseTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.PhaseTextBox_Validating);
            // 
            // _CyclesTextBox
            // 
            this._CyclesTextBox.AcceptsReturn = true;
            this._CyclesTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._CyclesTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._CyclesTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._CyclesTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._CyclesTextBox.Location = new System.Drawing.Point(280, 5);
            this._CyclesTextBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._CyclesTextBox.MaxLength = 0;
            this._CyclesTextBox.Name = "_CyclesTextBox";
            this._CyclesTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CyclesTextBox.Size = new System.Drawing.Size(55, 20);
            this._CyclesTextBox.TabIndex = 17;
            this._CyclesTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.CyclesTextBox_Validating);
            // 
            // _PointsTextBoxLabel
            // 
            this._PointsTextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._PointsTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._PointsTextBoxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._PointsTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._PointsTextBoxLabel.Location = new System.Drawing.Point(47, 7);
            this._PointsTextBoxLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._PointsTextBoxLabel.Name = "_PointsTextBoxLabel";
            this._PointsTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PointsTextBoxLabel.Size = new System.Drawing.Size(56, 18);
            this._PointsTextBoxLabel.TabIndex = 21;
            this._PointsTextBoxLabel.Text = "Points: ";
            this._PointsTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PhaseTextBoxLabel
            // 
            this._PhaseTextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._PhaseTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._PhaseTextBoxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._PhaseTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._PhaseTextBoxLabel.Location = new System.Drawing.Point(187, 37);
            this._PhaseTextBoxLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._PhaseTextBoxLabel.Name = "_PhaseTextBoxLabel";
            this._PhaseTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PhaseTextBoxLabel.Size = new System.Drawing.Size(93, 18);
            this._PhaseTextBoxLabel.TabIndex = 20;
            this._PhaseTextBoxLabel.Text = "Phase [Deg]: ";
            this._PhaseTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _CyclesTextBoxLabel
            // 
            this._CyclesTextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._CyclesTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._CyclesTextBoxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this._CyclesTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._CyclesTextBoxLabel.Location = new System.Drawing.Point(177, 7);
            this._CyclesTextBoxLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._CyclesTextBoxLabel.Name = "_CyclesTextBoxLabel";
            this._CyclesTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CyclesTextBoxLabel.Size = new System.Drawing.Size(102, 18);
            this._CyclesTextBoxLabel.TabIndex = 18;
            this._CyclesTextBoxLabel.Text = "Frequency [Hz]: ";
            this._CyclesTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SpectrumTabPage
            // 
            this._SpectrumTabPage.Controls.Add(this._SpectrumChartPanel);
            this._SpectrumTabPage.Controls.Add(this._SpectrumOptionsPanel);
            this._SpectrumTabPage.Location = new System.Drawing.Point(4, 24);
            this._SpectrumTabPage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._SpectrumTabPage.Name = "_SpectrumTabPage";
            this._SpectrumTabPage.Size = new System.Drawing.Size(645, 397);
            this._SpectrumTabPage.TabIndex = 1;
            this._SpectrumTabPage.Text = "Spectrum";
            // 
            // _SpectrumChartPanel
            // 
            this._SpectrumChartPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._SpectrumChartPanel.Location = new System.Drawing.Point(0, 59);
            this._SpectrumChartPanel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._SpectrumChartPanel.Name = "_SpectrumChartPanel";
            this._SpectrumChartPanel.Size = new System.Drawing.Size(645, 338);
            this._SpectrumChartPanel.TabIndex = 28;
            // 
            // _SpectrumOptionsPanel
            // 
            this._SpectrumOptionsPanel.Controls.Add(this._ExampleComboBoxLabel);
            this._SpectrumOptionsPanel.Controls.Add(this._StartStopCheckBox);
            this._SpectrumOptionsPanel.Controls.Add(this._SingleRadioButton);
            this._SpectrumOptionsPanel.Controls.Add(this._DoubleRadioButton);
            this._SpectrumOptionsPanel.Controls.Add(this._RemoveMeanCheckBox);
            this._SpectrumOptionsPanel.Controls.Add(this._ExampleComboBox);
            this._SpectrumOptionsPanel.Controls.Add(this._TaperWindowCheckBox);
            this._SpectrumOptionsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._SpectrumOptionsPanel.Location = new System.Drawing.Point(0, 0);
            this._SpectrumOptionsPanel.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._SpectrumOptionsPanel.Name = "_SpectrumOptionsPanel";
            this._SpectrumOptionsPanel.Size = new System.Drawing.Size(645, 59);
            this._SpectrumOptionsPanel.TabIndex = 27;
            // 
            // _ExampleComboBoxLabel
            // 
            this._ExampleComboBoxLabel.Location = new System.Drawing.Point(271, 33);
            this._ExampleComboBoxLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this._ExampleComboBoxLabel.Name = "_ExampleComboBoxLabel";
            this._ExampleComboBoxLabel.Size = new System.Drawing.Size(75, 18);
            this._ExampleComboBoxLabel.TabIndex = 32;
            this._ExampleComboBoxLabel.Text = "Calculate: ";
            this._ExampleComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _StartStopCheckBox
            // 
            this._StartStopCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._StartStopCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this._StartStopCheckBox.Location = new System.Drawing.Point(566, 29);
            this._StartStopCheckBox.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._StartStopCheckBox.Name = "_StartStopCheckBox";
            this._StartStopCheckBox.Size = new System.Drawing.Size(75, 28);
            this._StartStopCheckBox.TabIndex = 31;
            this._StartStopCheckBox.Text = "&Start";
            this._StartStopCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._StartStopCheckBox.CheckedChanged += new System.EventHandler(this.StartStopCheckBox_CheckedChanged);
            // 
            // _SingleRadioButton
            // 
            this._SingleRadioButton.Location = new System.Drawing.Point(131, 29);
            this._SingleRadioButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._SingleRadioButton.Name = "_SingleRadioButton";
            this._SingleRadioButton.Size = new System.Drawing.Size(121, 20);
            this._SingleRadioButton.TabIndex = 28;
            this._SingleRadioButton.Text = "Single Precision";
            // 
            // _MessagesTabPage
            // 
            this._MessagesTabPage.Controls.Add(this._NotesMessageList);
            this._MessagesTabPage.Location = new System.Drawing.Point(4, 24);
            this._MessagesTabPage.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._MessagesTabPage.Name = "_MessagesTabPage";
            this._MessagesTabPage.Size = new System.Drawing.Size(645, 397);
            this._MessagesTabPage.TabIndex = 3;
            this._MessagesTabPage.Text = "Messages";
            // 
            // _NotesMessageList
            // 
            this._NotesMessageList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._NotesMessageList.Location = new System.Drawing.Point(0, 0);
            this._NotesMessageList.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this._NotesMessageList.Multiline = true;
            this._NotesMessageList.Name = "_NotesMessageList";
            this._NotesMessageList.ReadOnly = true;
            this._NotesMessageList.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._NotesMessageList.Size = new System.Drawing.Size(645, 397);
            this._NotesMessageList.TabIndex = 4;
            // 
            // _ErrorErrorProvider
            // 
            this._ErrorErrorProvider.ContainerControl = this;
            // 
            // _CountStatusBarPanel
            // 
            this._CountStatusBarPanel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._CountStatusBarPanel.Name = "_CountStatusBarPanel";
            this._CountStatusBarPanel.Size = new System.Drawing.Size(13, 17);
            this._CountStatusBarPanel.Text = "0";
            this._CountStatusBarPanel.ToolTipText = "FFT counter";
            // 
            // _StatusStatusBarPanel
            // 
            this._StatusStatusBarPanel.Name = "_StatusStatusBarPanel";
            this._StatusStatusBarPanel.Size = new System.Drawing.Size(538, 17);
            this._StatusStatusBarPanel.Spring = true;
            this._StatusStatusBarPanel.Text = "<status>";
            this._StatusStatusBarPanel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._StatusStatusBarPanel.ToolTipText = "Status";
            // 
            // _MainStatusBar
            // 
            this._MainStatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._StatusStatusBarPanel,
            this._CountStatusBarPanel,
            this._ErrorStatusBarPanel,
            this._FftTimeStatusBarPanel});
            this._MainStatusBar.Location = new System.Drawing.Point(0, 428);
            this._MainStatusBar.Name = "_MainStatusBar";
            this._MainStatusBar.Size = new System.Drawing.Size(653, 22);
            this._MainStatusBar.TabIndex = 3;
            this._MainStatusBar.Text = "Main Status Bar";
            // 
            // WisdomSpectrumView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._MainTabControl);
            this.Controls.Add(this._MainStatusBar);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "WisdomSpectrumView";
            this.Size = new System.Drawing.Size(653, 450);
            this.Load += new System.EventHandler(this.Form_Load);
            this._DataTabPage.ResumeLayout(false);
            this._DataTabPage.PerformLayout();
            this._MainTabControl.ResumeLayout(false);
            this._SignalTabPage.ResumeLayout(false);
            this._SignalOptionsPanel.ResumeLayout(false);
            this._SignalOptionsPanel.PerformLayout();
            this._SpectrumTabPage.ResumeLayout(false);
            this._SpectrumOptionsPanel.ResumeLayout(false);
            this._MessagesTabPage.ResumeLayout(false);
            this._MessagesTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorErrorProvider)).EndInit();
            this._MainStatusBar.ResumeLayout(false);
            this._MainStatusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private TextBox _PointsToDisplayTextBox;
        private ListBox _FftListBox;
        private TabPage _DataTabPage;
        private Label _PointsToDisplayTextBoxLabel;
        private Label _FftListBoxLabel;
        private RadioButton _DoubleRadioButton;
        private CheckBox _RemoveMeanCheckBox;
        private ComboBox _ExampleComboBox;
        private CheckBox _TaperWindowCheckBox;
        private ToolStripStatusLabel _FftTimeStatusBarPanel;
        private ToolStripStatusLabel _ErrorStatusBarPanel;
        private TabControl _MainTabControl;
        private TabPage _SignalTabPage;
        private Panel _SignalChartPanel;
        private Panel _SignalOptionsPanel;
        private TextBox _SignalDurationTextBox;
        private Label _SignalDurationTextBoxLabel;
        private TextBox _PointsTextBox;
        private TextBox _PhaseTextBox;
        private TextBox _CyclesTextBox;
        private Label _PointsTextBoxLabel;
        private Label _PhaseTextBoxLabel;
        private Label _CyclesTextBoxLabel;
        private TabPage _SpectrumTabPage;
        private Panel _SpectrumChartPanel;
        private Panel _SpectrumOptionsPanel;
        private Label _ExampleComboBoxLabel;
        private CheckBox _StartStopCheckBox;
        private RadioButton _SingleRadioButton;
        private TabPage _MessagesTabPage;
        private TextBox _NotesMessageList;
        private ErrorProvider _ErrorErrorProvider;
        private StatusStrip _MainStatusBar;
        private ToolStripStatusLabel _StatusStatusBarPanel;
        private ToolStripStatusLabel _CountStatusBarPanel;
        private Label _TimingListBoxLabel;
        private TextBox _TimingTextBox;
    }
}
