using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.Signals.Demo
{
    public partial class SpectrumView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this._PointsToDisplayTextBox = new System.Windows.Forms.TextBox();
            this._SignalListBox = new System.Windows.Forms.ListBox();
            this._DataTabPage = new System.Windows.Forms.TabPage();
            this._TimingTextBox = new System.Windows.Forms.TextBox();
            this._TimingListBoxLabel = new System.Windows.Forms.Label();
            this._PointsToDisplayTextBoxLabel = new System.Windows.Forms.Label();
            this._SignalListBoxLabel = new System.Windows.Forms.Label();
            this._DoubleRadioButton = new System.Windows.Forms.RadioButton();
            this._RemoveMeanCheckBox = new System.Windows.Forms.CheckBox();
            this._ExampleComboBox = new System.Windows.Forms.ComboBox();
            this._TaperWindowCheckBox = new System.Windows.Forms.CheckBox();
            this._Tabs = new System.Windows.Forms.TabControl();
            this._SignalTabPage = new System.Windows.Forms.TabPage();
            this._SignalChartPanel = new System.Windows.Forms.Panel();
            this._SignalOptionsPanel = new System.Windows.Forms.Panel();
            this._SignalComboBoxLabel = new System.Windows.Forms.Label();
            this._SignalComboBox = new System.Windows.Forms.ComboBox();
            this._SignalDurationTextBox = new System.Windows.Forms.TextBox();
            this._SignalDurationTextBoxLabel = new System.Windows.Forms.Label();
            this._PointsTextBox = new System.Windows.Forms.TextBox();
            this._PhaseTextBox = new System.Windows.Forms.TextBox();
            this._CyclesTextBox = new System.Windows.Forms.TextBox();
            this._PointsTextBoxLabel = new System.Windows.Forms.Label();
            this._PhaseTextBoxLabel = new System.Windows.Forms.Label();
            this._CyclesTextBoxLabel = new System.Windows.Forms.Label();
            this._SpectrumTabPage = new System.Windows.Forms.TabPage();
            this._SpectrumChartPanel = new System.Windows.Forms.Panel();
            this._SpectrumOptionsPanel = new System.Windows.Forms.Panel();
            this._FilterComboBoxLabel = new System.Windows.Forms.Label();
            this._FilterComboBox = new System.Windows.Forms.ComboBox();
            this._SingleRadioButton = new System.Windows.Forms.RadioButton();
            this._ExampleComboBoxLabel = new System.Windows.Forms.Label();
            this._StartStopCheckBox = new System.Windows.Forms.CheckBox();
            this._MessagesTabPage = new System.Windows.Forms.TabPage();
            this._MessagesList = new System.Windows.Forms.TextBox();
            this._ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this._StatusStrip = new System.Windows.Forms.StatusStrip();
            this._StatusToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._CountToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._ErrorToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._TimeToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this._DataTabPage.SuspendLayout();
            this._Tabs.SuspendLayout();
            this._SignalTabPage.SuspendLayout();
            this._SignalOptionsPanel.SuspendLayout();
            this._SpectrumTabPage.SuspendLayout();
            this._SpectrumOptionsPanel.SuspendLayout();
            this._MessagesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).BeginInit();
            this._StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _PointsToDisplayTextBox
            // 
            this._PointsToDisplayTextBox.AcceptsReturn = true;
            this._PointsToDisplayTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._PointsToDisplayTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._PointsToDisplayTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._PointsToDisplayTextBox.Location = new System.Drawing.Point(129, 15);
            this._PointsToDisplayTextBox.MaxLength = 0;
            this._PointsToDisplayTextBox.Name = "_PointsToDisplayTextBox";
            this._PointsToDisplayTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PointsToDisplayTextBox.Size = new System.Drawing.Size(65, 23);
            this._PointsToDisplayTextBox.TabIndex = 1;
            this._PointsToDisplayTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.PointsToDisplayTextBox_Validating);
            // 
            // _SignalListBox
            // 
            this._SignalListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._SignalListBox.BackColor = System.Drawing.SystemColors.Window;
            this._SignalListBox.Cursor = System.Windows.Forms.Cursors.Default;
            this._SignalListBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._SignalListBox.ItemHeight = 15;
            this._SignalListBox.Location = new System.Drawing.Point(272, 24);
            this._SignalListBox.Name = "_SignalListBox";
            this._SignalListBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._SignalListBox.Size = new System.Drawing.Size(295, 289);
            this._SignalListBox.TabIndex = 5;
            // 
            // _DataTabPage
            // 
            this._DataTabPage.Controls.Add(this._TimingTextBox);
            this._DataTabPage.Controls.Add(this._TimingListBoxLabel);
            this._DataTabPage.Controls.Add(this._PointsToDisplayTextBox);
            this._DataTabPage.Controls.Add(this._SignalListBox);
            this._DataTabPage.Controls.Add(this._PointsToDisplayTextBoxLabel);
            this._DataTabPage.Controls.Add(this._SignalListBoxLabel);
            this._DataTabPage.Location = new System.Drawing.Point(4, 24);
            this._DataTabPage.Name = "_DataTabPage";
            this._DataTabPage.Size = new System.Drawing.Size(578, 340);
            this._DataTabPage.TabIndex = 2;
            this._DataTabPage.Text = "Data";
            // 
            // _TimingTextBox
            // 
            this._TimingTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this._TimingTextBox.Location = new System.Drawing.Point(16, 64);
            this._TimingTextBox.Multiline = true;
            this._TimingTextBox.Name = "_TimingTextBox";
            this._TimingTextBox.Size = new System.Drawing.Size(246, 255);
            this._TimingTextBox.TabIndex = 3;
            // 
            // _TimingListBoxLabel
            // 
            this._TimingListBoxLabel.AutoSize = true;
            this._TimingListBoxLabel.Location = new System.Drawing.Point(15, 45);
            this._TimingListBoxLabel.Name = "_TimingListBoxLabel";
            this._TimingListBoxLabel.Size = new System.Drawing.Size(77, 15);
            this._TimingListBoxLabel.TabIndex = 2;
            this._TimingListBoxLabel.Text = "Timing Data: ";
            // 
            // _PointsToDisplayTextBoxLabel
            // 
            this._PointsToDisplayTextBoxLabel.AutoSize = true;
            this._PointsToDisplayTextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._PointsToDisplayTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._PointsToDisplayTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._PointsToDisplayTextBoxLabel.Location = new System.Drawing.Point(16, 19);
            this._PointsToDisplayTextBoxLabel.Name = "_PointsToDisplayTextBoxLabel";
            this._PointsToDisplayTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PointsToDisplayTextBoxLabel.Size = new System.Drawing.Size(101, 15);
            this._PointsToDisplayTextBoxLabel.TabIndex = 0;
            this._PointsToDisplayTextBoxLabel.Text = "Points to Display: ";
            this._PointsToDisplayTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SignalListBoxLabel
            // 
            this._SignalListBoxLabel.AutoSize = true;
            this._SignalListBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._SignalListBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._SignalListBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._SignalListBoxLabel.Location = new System.Drawing.Point(274, 5);
            this._SignalListBoxLabel.Name = "_SignalListBoxLabel";
            this._SignalListBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._SignalListBoxLabel.Size = new System.Drawing.Size(252, 15);
            this._SignalListBoxLabel.TabIndex = 4;
            this._SignalListBoxLabel.Text = "Signal                       Complex IFFT{ FFT{ Signal}}";
            // 
            // _DoubleRadioButton
            // 
            this._DoubleRadioButton.AutoSize = true;
            this._DoubleRadioButton.Checked = true;
            this._DoubleRadioButton.Location = new System.Drawing.Point(124, 9);
            this._DoubleRadioButton.Name = "_DoubleRadioButton";
            this._DoubleRadioButton.Size = new System.Drawing.Size(114, 19);
            this._DoubleRadioButton.TabIndex = 0;
            this._DoubleRadioButton.TabStop = true;
            this._DoubleRadioButton.Text = "Double Precision";
            // 
            // _RemoveMeanCheckBox
            // 
            this._RemoveMeanCheckBox.AutoSize = true;
            this._RemoveMeanCheckBox.BackColor = System.Drawing.SystemColors.Control;
            this._RemoveMeanCheckBox.Cursor = System.Windows.Forms.Cursors.Default;
            this._RemoveMeanCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this._RemoveMeanCheckBox.Location = new System.Drawing.Point(8, 9);
            this._RemoveMeanCheckBox.Name = "_RemoveMeanCheckBox";
            this._RemoveMeanCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._RemoveMeanCheckBox.Size = new System.Drawing.Size(102, 19);
            this._RemoveMeanCheckBox.TabIndex = 0;
            this._RemoveMeanCheckBox.Text = "Remove Mean";
            this._RemoveMeanCheckBox.UseVisualStyleBackColor = false;
            // 
            // _ExampleComboBox
            // 
            this._ExampleComboBox.BackColor = System.Drawing.SystemColors.Window;
            this._ExampleComboBox.Cursor = System.Windows.Forms.Cursors.Default;
            this._ExampleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._ExampleComboBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._ExampleComboBox.Items.AddRange(new object[] {
            "Mixed Radix FFT",
            "Sliding FFT"});
            this._ExampleComboBox.Location = new System.Drawing.Point(395, 34);
            this._ExampleComboBox.Name = "_ExampleComboBox";
            this._ExampleComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._ExampleComboBox.Size = new System.Drawing.Size(178, 23);
            this._ExampleComboBox.TabIndex = 5;
            // 
            // _TaperWindowCheckBox
            // 
            this._TaperWindowCheckBox.AutoSize = true;
            this._TaperWindowCheckBox.BackColor = System.Drawing.SystemColors.Control;
            this._TaperWindowCheckBox.Cursor = System.Windows.Forms.Cursors.Default;
            this._TaperWindowCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this._TaperWindowCheckBox.Location = new System.Drawing.Point(8, 34);
            this._TaperWindowCheckBox.Name = "_TaperWindowCheckBox";
            this._TaperWindowCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._TaperWindowCheckBox.Size = new System.Drawing.Size(101, 19);
            this._TaperWindowCheckBox.TabIndex = 1;
            this._TaperWindowCheckBox.Text = "Taper Window";
            this._TaperWindowCheckBox.UseVisualStyleBackColor = false;
            // 
            // _Tabs
            // 
            this._Tabs.Controls.Add(this._SignalTabPage);
            this._Tabs.Controls.Add(this._SpectrumTabPage);
            this._Tabs.Controls.Add(this._DataTabPage);
            this._Tabs.Controls.Add(this._MessagesTabPage);
            this._Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Tabs.Location = new System.Drawing.Point(0, 0);
            this._Tabs.Name = "_Tabs";
            this._Tabs.SelectedIndex = 0;
            this._Tabs.Size = new System.Drawing.Size(586, 368);
            this._Tabs.TabIndex = 2;
            // 
            // _SignalTabPage
            // 
            this._SignalTabPage.Controls.Add(this._SignalChartPanel);
            this._SignalTabPage.Controls.Add(this._SignalOptionsPanel);
            this._SignalTabPage.Location = new System.Drawing.Point(4, 24);
            this._SignalTabPage.Name = "_SignalTabPage";
            this._SignalTabPage.Size = new System.Drawing.Size(578, 340);
            this._SignalTabPage.TabIndex = 0;
            this._SignalTabPage.Text = "Signal";
            // 
            // _SignalChartPanel
            // 
            this._SignalChartPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._SignalChartPanel.Location = new System.Drawing.Point(0, 60);
            this._SignalChartPanel.Name = "_SignalChartPanel";
            this._SignalChartPanel.Size = new System.Drawing.Size(578, 280);
            this._SignalChartPanel.TabIndex = 30;
            // 
            // _SignalOptionsPanel
            // 
            this._SignalOptionsPanel.Controls.Add(this._SignalComboBoxLabel);
            this._SignalOptionsPanel.Controls.Add(this._SignalComboBox);
            this._SignalOptionsPanel.Controls.Add(this._SignalDurationTextBox);
            this._SignalOptionsPanel.Controls.Add(this._SignalDurationTextBoxLabel);
            this._SignalOptionsPanel.Controls.Add(this._PointsTextBox);
            this._SignalOptionsPanel.Controls.Add(this._PhaseTextBox);
            this._SignalOptionsPanel.Controls.Add(this._CyclesTextBox);
            this._SignalOptionsPanel.Controls.Add(this._PointsTextBoxLabel);
            this._SignalOptionsPanel.Controls.Add(this._PhaseTextBoxLabel);
            this._SignalOptionsPanel.Controls.Add(this._CyclesTextBoxLabel);
            this._SignalOptionsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._SignalOptionsPanel.Location = new System.Drawing.Point(0, 0);
            this._SignalOptionsPanel.Name = "_SignalOptionsPanel";
            this._SignalOptionsPanel.Size = new System.Drawing.Size(578, 60);
            this._SignalOptionsPanel.TabIndex = 0;
            // 
            // _SignalComboBoxLabel
            // 
            this._SignalComboBoxLabel.AutoSize = true;
            this._SignalComboBoxLabel.Location = new System.Drawing.Point(370, 8);
            this._SignalComboBoxLabel.Name = "_SignalComboBoxLabel";
            this._SignalComboBoxLabel.Size = new System.Drawing.Size(42, 15);
            this._SignalComboBoxLabel.TabIndex = 8;
            this._SignalComboBoxLabel.Text = "Signal:";
            // 
            // _SignalComboBox
            // 
            this._SignalComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._SignalComboBox.FormattingEnabled = true;
            this._SignalComboBox.Location = new System.Drawing.Point(418, 4);
            this._SignalComboBox.Name = "_SignalComboBox";
            this._SignalComboBox.Size = new System.Drawing.Size(121, 23);
            this._SignalComboBox.TabIndex = 9;
            this._SignalComboBox.Validated += new System.EventHandler(this.SignalComboBox_Validated);
            // 
            // _SignalDurationTextBox
            // 
            this._SignalDurationTextBox.AcceptsReturn = true;
            this._SignalDurationTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._SignalDurationTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._SignalDurationTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._SignalDurationTextBox.Location = new System.Drawing.Point(107, 30);
            this._SignalDurationTextBox.MaxLength = 0;
            this._SignalDurationTextBox.Name = "_SignalDurationTextBox";
            this._SignalDurationTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._SignalDurationTextBox.Size = new System.Drawing.Size(46, 23);
            this._SignalDurationTextBox.TabIndex = 3;
            // 
            // _SignalDurationTextBoxLabel
            // 
            this._SignalDurationTextBoxLabel.AutoSize = true;
            this._SignalDurationTextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._SignalDurationTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._SignalDurationTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._SignalDurationTextBoxLabel.Location = new System.Drawing.Point(8, 34);
            this._SignalDurationTextBoxLabel.Name = "_SignalDurationTextBoxLabel";
            this._SignalDurationTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._SignalDurationTextBoxLabel.Size = new System.Drawing.Size(88, 15);
            this._SignalDurationTextBoxLabel.TabIndex = 2;
            this._SignalDurationTextBoxLabel.Text = "Duration [Sec]: ";
            this._SignalDurationTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PointsTextBox
            // 
            this._PointsTextBox.AcceptsReturn = true;
            this._PointsTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._PointsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._PointsTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._PointsTextBox.Location = new System.Drawing.Point(107, 4);
            this._PointsTextBox.MaxLength = 0;
            this._PointsTextBox.Name = "_PointsTextBox";
            this._PointsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PointsTextBox.Size = new System.Drawing.Size(46, 23);
            this._PointsTextBox.TabIndex = 1;
            this._PointsTextBox.Text = "1000";
            this._PointsTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.PointsTextBox_Validating);
            // 
            // _PhaseTextBox
            // 
            this._PhaseTextBox.AcceptsReturn = true;
            this._PhaseTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._PhaseTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._PhaseTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._PhaseTextBox.Location = new System.Drawing.Point(278, 30);
            this._PhaseTextBox.MaxLength = 0;
            this._PhaseTextBox.Name = "_PhaseTextBox";
            this._PhaseTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PhaseTextBox.Size = new System.Drawing.Size(48, 23);
            this._PhaseTextBox.TabIndex = 7;
            this._PhaseTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.PhaseTextBox_Validating);
            // 
            // _CyclesTextBox
            // 
            this._CyclesTextBox.AcceptsReturn = true;
            this._CyclesTextBox.BackColor = System.Drawing.SystemColors.Window;
            this._CyclesTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._CyclesTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this._CyclesTextBox.Location = new System.Drawing.Point(278, 4);
            this._CyclesTextBox.MaxLength = 0;
            this._CyclesTextBox.Name = "_CyclesTextBox";
            this._CyclesTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CyclesTextBox.Size = new System.Drawing.Size(48, 23);
            this._CyclesTextBox.TabIndex = 5;
            this._CyclesTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.CyclesTextBox_Validating);
            // 
            // _PointsTextBoxLabel
            // 
            this._PointsTextBoxLabel.AutoSize = true;
            this._PointsTextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._PointsTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._PointsTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._PointsTextBoxLabel.Location = new System.Drawing.Point(55, 8);
            this._PointsTextBoxLabel.Name = "_PointsTextBoxLabel";
            this._PointsTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PointsTextBoxLabel.Size = new System.Drawing.Size(46, 15);
            this._PointsTextBoxLabel.TabIndex = 0;
            this._PointsTextBoxLabel.Text = "Points: ";
            this._PointsTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PhaseTextBoxLabel
            // 
            this._PhaseTextBoxLabel.AutoSize = true;
            this._PhaseTextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._PhaseTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._PhaseTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._PhaseTextBoxLabel.Location = new System.Drawing.Point(191, 34);
            this._PhaseTextBoxLabel.Name = "_PhaseTextBoxLabel";
            this._PhaseTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._PhaseTextBoxLabel.Size = new System.Drawing.Size(76, 15);
            this._PhaseTextBoxLabel.TabIndex = 6;
            this._PhaseTextBoxLabel.Text = "Phase [Deg]: ";
            this._PhaseTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _CyclesTextBoxLabel
            // 
            this._CyclesTextBoxLabel.AutoSize = true;
            this._CyclesTextBoxLabel.BackColor = System.Drawing.SystemColors.Control;
            this._CyclesTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            this._CyclesTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this._CyclesTextBoxLabel.Location = new System.Drawing.Point(175, 8);
            this._CyclesTextBoxLabel.Name = "_CyclesTextBoxLabel";
            this._CyclesTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._CyclesTextBoxLabel.Size = new System.Drawing.Size(93, 15);
            this._CyclesTextBoxLabel.TabIndex = 4;
            this._CyclesTextBoxLabel.Text = "Frequency [Hz]: ";
            this._CyclesTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SpectrumTabPage
            // 
            this._SpectrumTabPage.Controls.Add(this._SpectrumChartPanel);
            this._SpectrumTabPage.Controls.Add(this._SpectrumOptionsPanel);
            this._SpectrumTabPage.Location = new System.Drawing.Point(4, 24);
            this._SpectrumTabPage.Name = "_SpectrumTabPage";
            this._SpectrumTabPage.Size = new System.Drawing.Size(578, 340);
            this._SpectrumTabPage.TabIndex = 1;
            this._SpectrumTabPage.Text = "Spectrum";
            // 
            // _SpectrumChartPanel
            // 
            this._SpectrumChartPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._SpectrumChartPanel.Location = new System.Drawing.Point(0, 64);
            this._SpectrumChartPanel.Name = "_SpectrumChartPanel";
            this._SpectrumChartPanel.Size = new System.Drawing.Size(578, 276);
            this._SpectrumChartPanel.TabIndex = 1;
            // 
            // _SpectrumOptionsPanel
            // 
            this._SpectrumOptionsPanel.Controls.Add(this._FilterComboBoxLabel);
            this._SpectrumOptionsPanel.Controls.Add(this._FilterComboBox);
            this._SpectrumOptionsPanel.Controls.Add(this._DoubleRadioButton);
            this._SpectrumOptionsPanel.Controls.Add(this._SingleRadioButton);
            this._SpectrumOptionsPanel.Controls.Add(this._ExampleComboBoxLabel);
            this._SpectrumOptionsPanel.Controls.Add(this._StartStopCheckBox);
            this._SpectrumOptionsPanel.Controls.Add(this._RemoveMeanCheckBox);
            this._SpectrumOptionsPanel.Controls.Add(this._ExampleComboBox);
            this._SpectrumOptionsPanel.Controls.Add(this._TaperWindowCheckBox);
            this._SpectrumOptionsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this._SpectrumOptionsPanel.Location = new System.Drawing.Point(0, 0);
            this._SpectrumOptionsPanel.Name = "_SpectrumOptionsPanel";
            this._SpectrumOptionsPanel.Size = new System.Drawing.Size(578, 64);
            this._SpectrumOptionsPanel.TabIndex = 0;
            // 
            // _FilterComboBoxLabel
            // 
            this._FilterComboBoxLabel.AutoSize = true;
            this._FilterComboBoxLabel.Location = new System.Drawing.Point(247, 15);
            this._FilterComboBoxLabel.Name = "_FilterComboBoxLabel";
            this._FilterComboBoxLabel.Size = new System.Drawing.Size(67, 15);
            this._FilterComboBoxLabel.TabIndex = 2;
            this._FilterComboBoxLabel.Text = "Taper Filter:";
            // 
            // _FilterComboBox
            // 
            this._FilterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._FilterComboBox.FormattingEnabled = true;
            this._FilterComboBox.Location = new System.Drawing.Point(247, 34);
            this._FilterComboBox.Name = "_FilterComboBox";
            this._FilterComboBox.Size = new System.Drawing.Size(141, 23);
            this._FilterComboBox.TabIndex = 3;
            // 
            // _SingleRadioButton
            // 
            this._SingleRadioButton.AutoSize = true;
            this._SingleRadioButton.Location = new System.Drawing.Point(125, 34);
            this._SingleRadioButton.Name = "_SingleRadioButton";
            this._SingleRadioButton.Size = new System.Drawing.Size(108, 19);
            this._SingleRadioButton.TabIndex = 1;
            this._SingleRadioButton.Text = "Single Precision";
            // 
            // _ExampleComboBoxLabel
            // 
            this._ExampleComboBoxLabel.AutoSize = true;
            this._ExampleComboBoxLabel.Location = new System.Drawing.Point(395, 14);
            this._ExampleComboBoxLabel.Name = "_ExampleComboBoxLabel";
            this._ExampleComboBoxLabel.Size = new System.Drawing.Size(62, 15);
            this._ExampleComboBoxLabel.TabIndex = 4;
            this._ExampleComboBoxLabel.Text = "Calculate: ";
            this._ExampleComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _StartStopCheckBox
            // 
            this._StartStopCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._StartStopCheckBox.Appearance = System.Windows.Forms.Appearance.Button;
            this._StartStopCheckBox.Location = new System.Drawing.Point(511, 9);
            this._StartStopCheckBox.Name = "_StartStopCheckBox";
            this._StartStopCheckBox.Size = new System.Drawing.Size(64, 24);
            this._StartStopCheckBox.TabIndex = 6;
            this._StartStopCheckBox.Text = "&Start";
            this._StartStopCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this._StartStopCheckBox.CheckedChanged += new System.EventHandler(this.StartStopCheckBox_CheckedChanged);
            // 
            // _MessagesTabPage
            // 
            this._MessagesTabPage.Controls.Add(this._MessagesList);
            this._MessagesTabPage.Location = new System.Drawing.Point(4, 24);
            this._MessagesTabPage.Name = "_MessagesTabPage";
            this._MessagesTabPage.Size = new System.Drawing.Size(578, 340);
            this._MessagesTabPage.TabIndex = 3;
            this._MessagesTabPage.Text = "Log";
            // 
            // _MessagesList
            // 
            this._MessagesList.BackColor = System.Drawing.SystemColors.Info;
            this._MessagesList.CausesValidation = false;
            this._MessagesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this._MessagesList.Location = new System.Drawing.Point(0, 0);
            this._MessagesList.Multiline = true;
            this._MessagesList.Name = "_MessagesList";
            this._MessagesList.ReadOnly = true;
            this._MessagesList.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._MessagesList.Size = new System.Drawing.Size(578, 340);
            this._MessagesList.TabIndex = 4;
            // 
            // _ErrorProvider
            // 
            this._ErrorProvider.ContainerControl = this;
            // 
            // _StatusStrip
            // 
            this._StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._StatusToolStripStatusLabel,
            this._CountToolStripStatusLabel,
            this._ErrorToolStripStatusLabel,
            this._TimeToolStripStatusLabel});
            this._StatusStrip.Location = new System.Drawing.Point(0, 368);
            this._StatusStrip.Name = "_StatusStrip";
            this._StatusStrip.Size = new System.Drawing.Size(586, 22);
            this._StatusStrip.TabIndex = 4;
            this._StatusStrip.Text = "StatusStrip1";
            // 
            // _StatusToolStripStatusLabel
            // 
            this._StatusToolStripStatusLabel.Name = "_StatusToolStripStatusLabel";
            this._StatusToolStripStatusLabel.Size = new System.Drawing.Size(471, 17);
            this._StatusToolStripStatusLabel.Spring = true;
            this._StatusToolStripStatusLabel.Text = "Ready";
            this._StatusToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._StatusToolStripStatusLabel.ToolTipText = "Status";
            // 
            // _CountToolStripStatusLabel
            // 
            this._CountToolStripStatusLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._CountToolStripStatusLabel.Name = "_CountToolStripStatusLabel";
            this._CountToolStripStatusLabel.Size = new System.Drawing.Size(13, 17);
            this._CountToolStripStatusLabel.Text = "0";
            this._CountToolStripStatusLabel.ToolTipText = "FFT counter";
            // 
            // _ErrorToolStripStatusLabel
            // 
            this._ErrorToolStripStatusLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._ErrorToolStripStatusLabel.Name = "_ErrorToolStripStatusLabel";
            this._ErrorToolStripStatusLabel.Size = new System.Drawing.Size(34, 17);
            this._ErrorToolStripStatusLabel.Text = "0.000";
            this._ErrorToolStripStatusLabel.ToolTipText = "RMS difference between signal and inverse FFT";
            // 
            // _TimeToolStripStatusLabel
            // 
            this._TimeToolStripStatusLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._TimeToolStripStatusLabel.Name = "_TimeToolStripStatusLabel";
            this._TimeToolStripStatusLabel.Size = new System.Drawing.Size(53, 17);
            this._TimeToolStripStatusLabel.Text = "0.000 ms";
            this._TimeToolStripStatusLabel.ToolTipText = "Time to calculate an FFT";
            // 
            // SpectrumView
            // 
            this.Controls.Add(this._Tabs);
            this.Controls.Add(this._StatusStrip);
            this.Name = "SpectrumView";
            this.Size = new System.Drawing.Size(586, 390);
            this.Load += new System.EventHandler(this.Form_Load);
            this._DataTabPage.ResumeLayout(false);
            this._DataTabPage.PerformLayout();
            this._Tabs.ResumeLayout(false);
            this._SignalTabPage.ResumeLayout(false);
            this._SignalOptionsPanel.ResumeLayout(false);
            this._SignalOptionsPanel.PerformLayout();
            this._SpectrumTabPage.ResumeLayout(false);
            this._SpectrumOptionsPanel.ResumeLayout(false);
            this._SpectrumOptionsPanel.PerformLayout();
            this._MessagesTabPage.ResumeLayout(false);
            this._MessagesTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._ErrorProvider)).EndInit();
            this._StatusStrip.ResumeLayout(false);
            this._StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private TextBox _PointsToDisplayTextBox;
        private ListBox _SignalListBox;
        private TabPage _DataTabPage;
        private Label _PointsToDisplayTextBoxLabel;
        private Label _SignalListBoxLabel;
        private RadioButton _DoubleRadioButton;
        private CheckBox _RemoveMeanCheckBox;
        private ComboBox _ExampleComboBox;
        private CheckBox _TaperWindowCheckBox;
        private TabControl _Tabs;
        private TabPage _SignalTabPage;
        private Panel _SignalChartPanel;
        private Panel _SignalOptionsPanel;
        private TextBox _SignalDurationTextBox;
        private Label _SignalDurationTextBoxLabel;
        private TextBox _PointsTextBox;
        private TextBox _PhaseTextBox;
        private TextBox _CyclesTextBox;
        private Label _PointsTextBoxLabel;
        private Label _PhaseTextBoxLabel;
        private Label _CyclesTextBoxLabel;
        private TabPage _SpectrumTabPage;
        private Panel _SpectrumChartPanel;
        private Panel _SpectrumOptionsPanel;
        private Label _ExampleComboBoxLabel;
        private CheckBox _StartStopCheckBox;
        private RadioButton _SingleRadioButton;
        private TabPage _MessagesTabPage;
        private TextBox _MessagesList;
        private ErrorProvider _ErrorProvider;
        private Label _TimingListBoxLabel;
        private TextBox _TimingTextBox;
        private Label _SignalComboBoxLabel;
        private ComboBox _SignalComboBox;
        private Label _FilterComboBoxLabel;
        private ComboBox _FilterComboBox;
        private StatusStrip _StatusStrip;
        private ToolStripStatusLabel _StatusToolStripStatusLabel;
        private ToolStripStatusLabel _CountToolStripStatusLabel;
        private ToolStripStatusLabel _ErrorToolStripStatusLabel;
        private ToolStripStatusLabel _TimeToolStripStatusLabel;
    }
}
