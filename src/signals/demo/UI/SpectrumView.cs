using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Windows.Forms;

using FastEnums;

using isr.Signals.Base;
using isr.Signals.Fourier;


namespace isr.Signals.Demo
{

    /// <summary>
    /// A user interface for calculating the spectrum using DFT, Mixed Radix, and sliding Fourier
    /// transforms.
    /// </summary>
    /// <remarks>
    /// David, 2005-11-17. from FFT Pro. <para>
    /// Launch this form by calling its Show or ShowDialog method from its default instance.
    /// </para><para>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.  </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public partial class SpectrumView : UserControl
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public SpectrumView() : base()
        {
            // Initialize user components that might be affected by resize or paint actions

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {

                    // Free managed resources when explicitly called
                    this._SignalSeries?.Dispose();
                    this._SignalSeries = null;
                    this._SignalChart?.ToggleDashedZoomRectangleMouseHandlers( false );
                    this._SignalChart?.ToggleContextMenu( false );
                    this._SignalChart?.Dispose();
                    this._SignalChart = null;

                    this._SpectrumSeries?.Dispose();
                    this._SpectrumSeries = null;
                    this._SpectrumChart?.ToggleDashedZoomRectangleMouseHandlers( false );
                    this._SpectrumChart?.ToggleContextMenu( false );
                    this._SpectrumChart?.Dispose();
                    this._SpectrumChart = null;
                    this.components?.Dispose();
                }
            }

            // Free shared unmanaged resources

            finally
            {
                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the selected example. </summary>
        /// <value> The selected example. </value>
        private Example SelectedExample => ( Example ) (( KeyValuePair<Enum, string> ) this._ExampleComboBox.SelectedItem).Key;

        /// <summary> Gets the selected SignalType. </summary>
        /// <value> The type of the selected signal. </value>
        private SignalType SelectedSignalType => ( SignalType ) (( KeyValuePair<Enum, string> ) this._SignalComboBox.SelectedItem).Key;

        /// <summary> Gets the selected TaperFilterType. </summary>
        /// <value> The type of the selected taper filter. </value>
        private isr.Signals.TaperFilter.TaperFilterType SelectedTaperFilterType => ( isr.Signals.TaperFilter.TaperFilterType ) (( KeyValuePair<Enum, string> ) this._FilterComboBox.SelectedItem).Key;

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Occurs when the form is loaded. </summary>
        /// <remarks>
        /// Use this method for doing any final initialization right before the form is shown. This is a
        /// good place to change the Visible and ShowInTaskbar properties to start the form as hidden.
        /// Starting a form as hidden is useful for forms that need to be running but that should not
        /// show themselves right away, such as forms with a notify icon in the task bar.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.EventArgs"/> </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // set the form caption
                this.Text = "SPECTRUM VIEW";

                // Initialize and set the user interface
                this.InitializeUserInterface();
            }

            // turn on the loaded flag
            // loaded = True

            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " USER INTERFACE "

        /// <summary> Shows the status. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="message"> The message. </param>
        private void ShowStatus( string message )
        {
            this._MessagesList.AppendText( message );
            this._StatusToolStripStatusLabel.Text = message;
        }

        /// <summary>Gets or sets the data format</summary>
        private const string _ListFormat = "{0:0.000000000000000}{1}{2:0.000000000000000}";

        /// <summary> Enumerates the action options. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private enum Example
        {

            /// <summary> An enum constant representing the discrete Fourier transform option. </summary>
            [System.ComponentModel.Description( "Discrete Fourier Transform" )]
            DiscreteFourierTransform,

            /// <summary> An enum constant representing the sliding Fourier transform option. </summary>
            [System.ComponentModel.Description( "Sliding FFT" )]
            SlidingFFT,

            /// <summary> An enum constant representing the Mixed Radix Fourier transform option. </summary>
            [System.ComponentModel.Description( "Mixed Radix FFT" )]
            MixedRadixFFT,

            /// <summary> An enum constant representing the Wisdom (FFTW) transform option. </summary>
            [System.ComponentModel.Description( "Wisdom FFT" )]
            WisdomFFT
        }

        /// <summary> Enumerates the signal options. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private enum SignalType
        {

            /// <summary> An enum constant representing the sine wave option. </summary>
            [System.ComponentModel.Description( "Sine" )]
            SineWave,

            /// <summary> An enum constant representing the random option. </summary>
            [System.ComponentModel.Description( "Random" )]
            Random
        }

        /// <summary> Initializes the user interface and tool tips. </summary>
        /// <remarks> Call this method from the form load method to set the user interface. </remarks>
        private void InitializeUserInterface()
        {

            // tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
            // tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")

            // Set initial values defining the signal
            this._PointsTextBox.Text = "1000";
            this._CyclesTextBox.Text = "1"; // "11.5"
            this._PhaseTextBox.Text = "0"; // "45"
            this._PointsToDisplayTextBox.Text = "100";
            this._SignalDurationTextBox.Text = "1";

            // set the default sample
            this.PopulateComboBoxs();

            // create the two charts.
            this.CreateSpectrumChart( 1, 0, 100, 1 );
            this.CreateSignalChart( 0, -1, 1, 1 );

            // plot the signal
            this.UpdateSignal();
        }

        #endregion

        #region " SIGNALS "

        /// <summary> Updates the signal. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0045:Convert to conditional expression", Justification = "<Pending>" )]
        private void UpdateSignal()
        {
            if ( this._DoubleRadioButton.Checked )
            {
                _ = this.UpdateSignalDouble();
            }
            else
            {
                _ = this.UpdateSignalSingle();
            }
        }

        /// <summary> Gets the signal Single. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The signal. </returns>
        private float[] GetSignalSingle()
        {
            float[] signal;
            switch ( this.SelectedSignalType )
            {
                case SignalType.Random:
                    {
                        signal = this.RandomWaveSingle();
                        break;
                    }

                default:
                    {
                        signal = this.SineWaveSingle();
                        break;
                    }
            }

            return signal;
        }

        /// <summary> Updates the signal single. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A new signal. </returns>
        private float[] UpdateSignalSingle()
        {
            var signal = this.GetSignalSingle();
            // Plot the Signal 
            float duration = float.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
            var frequences = isr.Signals.Waveform.Signal.Ramp( duration / signal.Length, signal.Length );
            this.ChartSignal( frequences, signal );
            return signal;
        }

        /// <summary> Gets the signal double. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The signal. </returns>
        private double[] GetSignalDouble()
        {
            double[] signal;
            switch ( this.SelectedSignalType )
            {
                case SignalType.Random:
                    {
                        signal = this.RandomWaveDouble();
                        break;
                    }

                default:
                    {
                        signal = this.SineWaveDouble();
                        break;
                    }
            }

            return signal;
        }

        /// <summary> Updates the signal double. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The signal. </returns>
        private double[] UpdateSignalDouble()
        {
            var signal = this.GetSignalDouble();
            // Plot the Signal 
            double duration = double.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
            var frequences = isr.Signals.Waveform.Signal.Ramp( duration / signal.Length, signal.Length );
            this.ChartSignal( frequences, signal );
            return signal;
        }

        /// <summary> Calculates the sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The sine wave signal. </returns>
        private double[] SineWaveDouble()
        {

            // Read number of points from the text box.
            int signalPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            double signalCycles = double.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
            double signalPhase = isr.Signals.Waveform.Signal.ToRadians( double.Parse( this._PhaseTextBox.Text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture ) );
            // get the signal
            return isr.Signals.Waveform.Signal.Sine( signalCycles, signalPhase, signalPoints );
        }

        /// <summary> Calculates the sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The sine wave signal. </returns>
        private float[] SineWaveSingle()
        {

            // Read number of points from the text box.
            int signalPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            float signalCycles = float.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
            double signalPhase = isr.Signals.Waveform.Signal.ToRadians( double.Parse( this._PhaseTextBox.Text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture ) );
            // get the signal
            return isr.Signals.Waveform.Signal.Sine( signalCycles, ( float ) signalPhase, signalPoints );
        }

        /// <summary> Calculates the Random wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The random signal. </returns>
        private double[] RandomWaveDouble()
        {

            // Read number of points from the text box.
            int dataPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
            return isr.Signals.Waveform.Signal.Random( 1.0d + DateTimeOffset.Now.Second, dataPoints );
        }

        /// <summary> Calculates the Random wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> The random signal. </returns>
        private float[] RandomWaveSingle()
        {

            // Read number of points from the text box.
            int dataPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
            var signal = isr.Signals.Waveform.Signal.Random( 1.0f + DateTimeOffset.Now.Second, dataPoints );
            return signal;
        }

        /// <summary> The copy signal format. </summary>
        private const string _CopySigFormat = "    Copy Signal:  {0:0.###} ms ";

        /// <summary> The FFT initialize format. </summary>
        private const string _FftInitFormat = " FFT Initialize:  {0:0} ms ";

        /// <summary> The FFT calculate format. </summary>
        private const string _FftCalcFormat = "  FFT Calculate:  {0:0.###} ms ";

        /// <summary> The frequency calculate format. </summary>
        private const string _FrqCalcFormat = "FFT Frequencies:  {0:0.###} ms ";

        /// <summary> The magnitude calculate format. </summary>
        private const string _MagCalcFormat = "  FFT Magnitude:  {0:0.###} ms ";

        /// <summary> The inverse calculate format. </summary>
        private const string _InvCalcFormat = "    Inverse FFT:  {0:0.###} ms ";

        /// <summary> The charting format. </summary>
        private const string _ChartingFormat = "  Charting time:  {0:0} ms ";

        #endregion

        #region " SPECTRUM "

        /// <summary>
        /// Calculates the Spectrum based on the selected algorithm and displays the results.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="algorithm"> The algorithm. </param>
        private void CalculateSpectrumDouble( Example algorithm )
        {
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            TimeSpan duration;
            var timingTextBuilder = new System.Text.StringBuilder();
            _ = timingTextBuilder.AppendLine( algorithm.Description() );
            this.ShowStatus( $"Calculating double-precision {algorithm.Description()} FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create and plot the signal
            var signal = this.UpdateSignalDouble();

            // Allocate the spectrum array.
            var fftValues = new Complex[fftPoints];
            signal.CopyTo( fftValues );

            // ------------ SELECT PROPERTIES ---------------

            // select the spectrum type
            FourierTransformBase fft = null;
            try
            {
                switch ( algorithm )
                {
                    case Example.DiscreteFourierTransform:
                        {
                            fft = new DiscreteFourierTransform();
                            break;
                        }

                    case Example.MixedRadixFFT:
                        {
                            fft = new isr.Signals.MetaNumerics.MixedRadixFourierTransform();
                            break;
                        }

                    case Example.WisdomFFT:
                        {
                            fft = new Wisdom.Dft();
                            break;
                        }

                    default:
                        {
                            return;
                        }
                }

                using var spectrum = new isr.Signals.Spectrum.Spectrum( fft ) {

                    // scale the FFT by the Window power and data points
                    IsScaleFft = true,

                    // Remove mean before calculating the FFT
                    IsRemoveMean = this._RemoveMeanCheckBox.Checked,

                    // Use Taper Window as selected
                    TaperWindow = this._TaperWindowCheckBox.Checked ? new isr.Signals.TaperWindow.BlackmanTaperWindow() : null,

                    SamplingRate = fftPoints

                };
                const double transitionBand = 0.01d;
                const double lowFreq = 0.2d;
                const double highFreq = 0.4d;
                spectrum.TaperFilter = new isr.Signals.TaperFilter.Filter( this.SelectedTaperFilterType ) {
                    TimeSeriesLength = fftPoints,
                    LowFrequency = spectrum.SamplingRate * lowFreq,
                    HighFrequency = spectrum.SamplingRate * highFreq,
                    TransitionBand = spectrum.SamplingRate * transitionBand
                };

                switch ( this.SelectedTaperFilterType )
                {
                    case isr.Signals.TaperFilter.TaperFilterType.BandPass:
                        {
                            break;
                        }

                    case isr.Signals.TaperFilter.TaperFilterType.BandReject:
                        {
                            break;
                        }

                    case isr.Signals.TaperFilter.TaperFilterType.HighPass:
                        {
                            break;
                        }

                    case isr.Signals.TaperFilter.TaperFilterType.LowPass:
                        {
                            break;
                        }

                    default:
                        {
                            break;
                        }
                }

                // Initialize the FFT.
                timeKeeper.Restart();
                spectrum.Initialize( fftValues );
                duration = timeKeeper.Elapsed;
                _ = timingTextBuilder.AppendFormat( _FftInitFormat, duration.TotalMilliseconds );
                _ = timingTextBuilder.AppendLine();

                // ------------ CREATE the SIGNAL ---------------

                timeKeeper.Restart();
                signal.CopyTo( fftValues );
                duration = timeKeeper.Elapsed;
                _ = timingTextBuilder.AppendFormat( _CopySigFormat, duration.TotalMilliseconds );
                _ = timingTextBuilder.AppendLine();

                // ------------ Calculate Forward and Inverse FFT ---------------

                // Compute the FFT.
                timeKeeper.Restart();
                spectrum.Calculate( fftValues );
                duration = timeKeeper.Elapsed;
                this._TimeToolStripStatusLabel.Text = $"{duration.TotalMilliseconds:0.###} ms";
                _ = timingTextBuilder.AppendFormat( _FftCalcFormat, duration.TotalMilliseconds );
                _ = timingTextBuilder.AppendLine();

                // ------- Calculate FFT outcomes ---------

                // Get the FFT magnitudes
                double[] magnitudes;
                magnitudes = fftValues.Magnitudes();
                timeKeeper.Restart();
                magnitudes = fftValues.Magnitudes();
                duration = timeKeeper.Elapsed;
                _ = timingTextBuilder.AppendFormat( _MagCalcFormat, duration.TotalMilliseconds );
                _ = timingTextBuilder.AppendLine();

                // Get the Frequency
                double[] frequencies;
                timeKeeper.Restart();
                frequencies = isr.Signals.Waveform.Signal.Frequencies( 1.0d, fftPoints );
                duration = timeKeeper.Elapsed;
                _ = timingTextBuilder.AppendFormat( _FrqCalcFormat, duration.TotalMilliseconds );
                _ = timingTextBuilder.AppendLine();

                // Compute the inverse transform.
                timeKeeper.Restart();
                fft.Inverse( fftValues );
                duration = timeKeeper.Elapsed;
                _ = timingTextBuilder.AppendFormat( _InvCalcFormat, duration.TotalMilliseconds );
                _ = timingTextBuilder.AppendLine();

                // Display the forward and inverse data.
                this.DisplayCalculationAccuracy( signal, fftValues );

                // ------------ Plot FFT outcome ---------------
                timeKeeper.Restart();
                this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1d );
                duration = timeKeeper.Elapsed;
                _ = timingTextBuilder.AppendFormat( _ChartingFormat, duration.TotalMilliseconds );
                _ = timingTextBuilder.AppendLine();
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( fft is object )
                {
                    fft.Dispose();
                }

                this._TimingTextBox.Text = timingTextBuilder.ToString();
            }
        }

        /// <summary>
        /// Calculates the Spectrum based on the selected algorithm and displays the results.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="algorithm"> The algorithm. </param>
        private void CalculateSpectrumSingle( Example algorithm )
        {
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            TimeSpan duration;
            var timingTextBuilder = new System.Text.StringBuilder();
            _ = timingTextBuilder.AppendLine( algorithm.Description() );
            this.ShowStatus( $"Calculating single-precision {algorithm.Description()} FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create and plot the signal
            var signal = this.UpdateSignalSingle();

            // Allocate the spectrum array.
            var fftValues = new Complex[fftPoints];
            signal.CopyTo( fftValues );

            // ------------ Calculate Forward and Inverse FFT ---------------

            FourierTransformBase fft = null;
            try
            {
                switch ( algorithm )
                {
                    case Example.DiscreteFourierTransform:
                        {
                            fft = new DiscreteFourierTransform();
                            break;
                        }

                    case Example.MixedRadixFFT:
                        {
                            fft = new isr.Signals.MetaNumerics.MixedRadixFourierTransform();
                            break;
                        }

                    case Example.WisdomFFT:
                        {
                            fft = new Wisdom.Dft();
                            break;
                        }

                    default:
                        {
                            return;
                        }
                }

                using var spectrum = new isr.Signals.Spectrum.Spectrum( fft ) {

                    // scale the FFT by the Window power and data points
                    IsScaleFft = true,

                    // Remove mean before calculating the FFT
                    IsRemoveMean = this._RemoveMeanCheckBox.Checked,

                    // Use Taper Window as selected
                    TaperWindow = this._TaperWindowCheckBox.Checked ? new isr.Signals.TaperWindow.BlackmanTaperWindow() : null
                };


                // Initialize the FFT.
                timeKeeper.Restart();
                spectrum.Initialize( fftValues );
                duration = timeKeeper.Elapsed;
                _ = timingTextBuilder.AppendFormat( _FftInitFormat, duration.TotalMilliseconds );
                _ = timingTextBuilder.AppendLine();

                // Compute the FFT.
                timeKeeper.Restart();
                spectrum.Calculate( fftValues );
                duration = timeKeeper.Elapsed;

                // Display time
                this._TimeToolStripStatusLabel.Text = $"{duration.TotalMilliseconds:0.###} ms ";

                // ------- Calculate FFT outcomes ---------

                // Get the FFT magnitudes
                double[] magnitudes;
                magnitudes = fftValues.Magnitudes();

                // Get the Frequency
                double[] frequencies;
                frequencies = isr.Signals.Waveform.Signal.Frequencies( 1.0d, fftPoints );

                // Compute the inverse transform.
                fft.Inverse( fftValues );

                // Display the forward and inverse data.
                this.DisplayCalculationAccuracy( signal, fftValues );

                // ------------ Plot FFT outcome ---------------
                this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1d );
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( fft is object )
                    fft.Dispose();
                this._TimingTextBox.Text = timingTextBuilder.ToString();
            }
        }

        /// <summary> Displays the test results. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="signal">     The signal. </param>
        /// <param name="timeSeries"> The time series. </param>
        private void DisplayCalculationAccuracy( double[] signal, Complex[] timeSeries )
        {
            if ( signal is null )
            {
                return;
            }

            if ( timeSeries is null )
            {
                return;
            }

            int elementCount = Math.Min( timeSeries.Length, int.Parse( this._PointsToDisplayTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
            if ( elementCount <= 0 )
            {
                return;
            }

            // Display the forward and inverse data.
            double totalError = 0d;
            this._SignalListBox.Items.Clear();
            double value;
            for ( int i = 0, loopTo = elementCount - 1; i <= loopTo; i++ )
            {
                value = signal[i] - timeSeries[i].Real;
                totalError += value * value;
                _ = this._SignalListBox.Items.Add( string.Format( System.Globalization.CultureInfo.CurrentCulture, _ListFormat, signal[i], "\t", timeSeries[i] ) );
            }

            totalError = Math.Sqrt( totalError / Convert.ToDouble( elementCount ) );
            this._ErrorToolStripStatusLabel.Text = $"{totalError:0.###E+0}";
            this._StatusStrip.Invalidate();
        }

        /// <summary> Displays the test results. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="signal">     The signal. </param>
        /// <param name="timeSeries"> The time series. </param>
        private void DisplayCalculationAccuracy( float[] signal, Complex[] timeSeries )
        {
            if ( signal is null )
            {
                return;
            }

            if ( timeSeries is null )
            {
                return;
            }

            int elementCount = Math.Min( timeSeries.Length, int.Parse( this._PointsToDisplayTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
            if ( elementCount <= 0 )
            {
                return;
            }

            // Display the forward and inverse data.
            double totalError = 0d;
            this._SignalListBox.Items.Clear();
            double value;
            for ( int i = 0, loopTo = elementCount - 1; i <= loopTo; i++ )
            {
                value = signal[i] - timeSeries[i].Real;
                totalError += value * value;
                _ = this._SignalListBox.Items.Add( string.Format( System.Globalization.CultureInfo.CurrentCulture, _ListFormat, signal[i], "\t", timeSeries[i] ) );
            }

            totalError = Math.Sqrt( totalError / Convert.ToDouble( elementCount ) );
            this._ErrorToolStripStatusLabel.Text = $"{totalError:0.###E+0}";
            this._StatusStrip.Invalidate();
        }

        /// <summary> Populates the list of options in the action combo box. </summary>
        /// <remarks> It seems that out enumerated list does not work very well with this list. </remarks>
        private void PopulateComboBoxs()
        {

            // set the action list
            this._ExampleComboBox.Items.Clear();
            this._ExampleComboBox.DataSource = typeof( Example ).ValueDescriptionPairs().ToList();
            this._ExampleComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._ExampleComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._ExampleComboBox.SelectedIndex = 0;
            this._SignalComboBox.Items.Clear();
            this._SignalComboBox.DataSource = typeof( SignalType ).ValueDescriptionPairs().ToList();
            this._SignalComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._SignalComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._SignalComboBox.SelectedIndex = 0;
            this._FilterComboBox.Items.Clear();
            this._FilterComboBox.DataSource = typeof( isr.Signals.TaperFilter.TaperFilterType ).ValueDescriptionPairs().ToList();
            this._FilterComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._FilterComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._FilterComboBox.SelectedIndex = 0;
        }

        /// <summary> Evaluates sliding FFT until stopped. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
        /// observing the changes in the spectrum. </param>
        private void SlidingFft( double noiseFigure )
        {
            this.ShowStatus( "Calculating double-precision sliding FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            var signal = this.SineWaveDouble();

            // Allocate the spectrum array.
            var fftValues = new Complex[fftPoints];
            signal.CopyTo( fftValues );

            // ------------ Calculate Forward and Inverse FFT ---------------

            var duration = TimeSpan.Zero;
            // Create a new instance of the Mixed Radix class
            using ( var fft = new isr.Signals.MetaNumerics.MixedRadixFourierTransform() )
            {
                // Compute the FFT.
                Stopwatch timeKeeper;
                timeKeeper = new Stopwatch();
                timeKeeper.Restart();
                fft.Forward( fftValues );
                duration = timeKeeper.Elapsed;
            }

            // Display time
            this._TimeToolStripStatusLabel.Text = $"{duration.TotalMilliseconds:0.###} ms";

            // Clear the data
            this._SignalListBox.Items.Clear();

            // Clear the error value
            this._ErrorToolStripStatusLabel.Text = string.Empty;

            // Gets the FFT magnitudes
            double[] magnitudes;

            // Gets the Frequencies
            double[] frequencies;

            // Initialize the last point of the signal to the last point.
            int lastSignalPoint = fftPoints - 1;

            // Initialize the first point of the signal to the last point.
            int firstSignalPoint = 0;

            // Calculate the phase of the last point of the sine wave
            double deltaPhase = 2d * Math.PI * float.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / Convert.ToSingle( fftPoints );

            // get the signal phase of the last point
            double signalPhase = isr.Signals.Waveform.Signal.ToRadians( double.Parse( this._PhaseTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
            signalPhase += deltaPhase * lastSignalPoint;

            // Use a new frequency for the sine wave to see how
            // the old is phased out and the new gets in
            deltaPhase *= 2d;

            // First element in the previous value of the time series
            Complex oldValue;

            // New value from the Signal
            Complex newValue;

            // Create a new instance of the sliding FFT class
            using var slidingFft = new SlidingFourierTransform();

            // Initialize sliding FFT coefficients.
            slidingFft.Forward( fftValues );

            Random rnd = new( DateTime.Now.Second );

            // Recalculate the sliding FFT
            while ( this._StartStopCheckBox.Checked )
            {

                // Allow other events to occur
                Application.DoEvents();

                // ------- Calculate FFT outcomes ---------

                // Get the FFT magnitudes
                magnitudes = slidingFft.Dft().Magnitudes();

                // Get the Frequency
                frequencies = isr.Signals.Waveform.Signal.Frequencies( 1.0d, fftPoints );

                // ------------ Plot the Signal ---------------

                this.ChartSignal( isr.Signals.Waveform.Signal.Ramp( double.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / fftPoints, fftPoints ), signal );

                // ------------ Plot FFT outcome ---------------

                this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1d );

                // Update the previous values of the signal
                oldValue = new Complex( signal[firstSignalPoint], 0d );

                // Add some random noise to make it interesting.
                double noise = noiseFigure * 2d * (rnd.NextDouble() - 0.5d);

                // Get new signal values.
                signalPhase += deltaPhase;
                newValue = new Complex( Math.Sin( signalPhase ) + noise, 0d );

                // Update the signal itself.

                // Update the last point.
                lastSignalPoint += 1;
                if ( lastSignalPoint >= fftPoints )
                {
                    lastSignalPoint = 0;
                }

                // Update the first point.
                firstSignalPoint += 1;
                if ( firstSignalPoint >= fftPoints )
                {
                    firstSignalPoint = 0;
                }

                // Place new data in the signal itself.
                signal[lastSignalPoint] = newValue.Real;

                // Calculate the sliding FFT coefficients.
                slidingFft.Update( newValue, oldValue );
            }
        }

        /// <summary> Evaluates sliding FFT until stopped. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
        /// observing the changes in the spectrum. </param>
        private void SlidingFft( float noiseFigure )
        {
            this.ShowStatus( "Calculating single-precision sliding FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create and plot the signal
            var signal = this.UpdateSignalSingle();

            // Allocate the spectrum array.
            var fftValues = new Complex[fftPoints];
            signal.CopyTo( fftValues );

            // ------------ Calculate Forward and Inverse FFT ---------------

            // Create a new instance of the Mixed Radix class
            using var fft = new isr.Signals.MetaNumerics.MixedRadixFourierTransform();

            // Compute the FFT.
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            timeKeeper.Restart();
            fft.Forward( fftValues );
            var duration = timeKeeper.Elapsed;

            // Display time
            this._TimeToolStripStatusLabel.Text = $"{duration.TotalMilliseconds:0.###} ms";

            // Clear the data
            this._SignalListBox.Items.Clear();
            this._ErrorToolStripStatusLabel.Text = string.Empty;

            // Get the FFT magnitudes
            double[] magnitudes;
            magnitudes = fftValues.Magnitudes();

            // Get the Frequency
            double[] frequencies;
            frequencies = isr.Signals.Waveform.Signal.Frequencies( 1.0d, fftPoints );

            // Initialize the last point of the signal to the last point.
            int lastSignalPoint = fftPoints - 1;

            // Initialize the first point of the signal to the last point.
            int firstSignalPoint = 0;

            // Calculate the phase of the last point of the sine wave
            float deltaPhase = 2.0f * Convert.ToSingle( Math.PI ) * float.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / fftPoints;

            // get the signal phase of the last point
            float signalPhase = Convert.ToSingle( isr.Signals.Waveform.Signal.ToRadians( float.Parse( this._PhaseTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) ) );
            signalPhase += deltaPhase * lastSignalPoint;

            // Use a new frequency for the sine wave to see how
            // the old is phased out and the new gets in
            deltaPhase *= 2f;

            // First element in the previous value of the time series
            Complex oldValue;

            // New value from the Signal
            Complex newValue;

            // Create a new instance of the sliding FFT class
            using var slidingFft = new SlidingFourierTransform();

            // Initialize sliding FFT coefficients.
            slidingFft.Forward( fftValues );

            Random rnd = new( DateTime.Now.Second );

            // Recalculate the sliding FFT
            while ( this._StartStopCheckBox.Checked )
            {

                // Allow other events to occur
                Application.DoEvents();

                // ------- Calculate FFT outcomes ---------

                // Get the FFT magnitudes
                magnitudes = slidingFft.Dft().Magnitudes();

                // Get the Frequency
                frequencies = isr.Signals.Waveform.Signal.Frequencies( 1.0d, fftPoints );

                // ------------ Plot the Signal ---------------

                this.ChartSignal( isr.Signals.Waveform.Signal.Ramp( float.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / fftPoints, fftPoints ), signal );

                // ------------ Plot FFT outcome ---------------

                this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1d );

                // Update the previous values of the signal
                oldValue = new Complex( signal[firstSignalPoint], 0d );

                // Add some random noise to make it interesting.
                double noise = noiseFigure * 2.0f * (rnd.NextDouble() - 0.5f);

                // Get new signal values.
                signalPhase += deltaPhase;
                newValue = new Complex( Math.Sin( signalPhase ) + noise, 0d );

                // Update the last point.
                lastSignalPoint += 1;
                if ( lastSignalPoint >= fftPoints )
                {
                    lastSignalPoint = 0;
                }

                // Update the first point.
                firstSignalPoint += 1;
                if ( firstSignalPoint >= fftPoints )
                {
                    firstSignalPoint = 0;
                }

                // Place new data in the signal itself.
                signal[lastSignalPoint] = ( float ) newValue.Real;

                // Calculate the sliding FFT coefficients.
                slidingFft.Update( newValue, oldValue );
            }
        }

        #endregion

        #region " AMPLITUDE SPECTRUM CHART "

        private isr.Visuals.Charting.LineChartControl _SpectrumChart;

        private System.Windows.Forms.DataVisualization.Charting.Series _SpectrumSeries;

        /// <summary> Creates the chart for displaying the amplitude spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private void CreateSpectrumChart( double abscissaMin, double ordinateMin, double abscissaMax, double ordinateMax )
        {

            if ( this._SpectrumSeries is object )
            {
                _ = this._SpectrumChart.Series.Remove( this._SpectrumSeries );
                this._SpectrumSeries.Dispose();
            }

            if ( this._SpectrumChart is not object )
            {
                this._SpectrumChart = new();
                this._SpectrumChartPanel.Controls.Add( this._SpectrumChart );
                this._SpectrumChart.Dock = DockStyle.Fill;
                this._SpectrumChart.InitializeKnownState( false );
            }
            this._SpectrumSeries = this._SpectrumChart.SetupLineChartChart( "spectrum", abscissaMin, ordinateMin,
                                                                            abscissaMax, ordinateMax,
                                                                            "Frequency, Hz", "Amplitude, Volts" );
            // not needed: this._SpectrumChart.AddTitle( "Spectrum" );
            this._SpectrumChart.ToggleDashedZoomRectangleMouseHandlers( true );
            this._SpectrumChart.PopulateContextMenu();
            this._SpectrumChart.ToggleContextMenu( true );
        }

        /// <summary>
        /// Plots the amplitude spectrum using the spectrum amplitudes and frequencies.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequencies"> Holds the spectrum frequencies. </param>
        /// <param name="magnitudes">  Holds the spectrum amplitudes. </param>
        /// <param name="scaleFactor"> Specifies the scale factor by which to scale the spectrum for
        /// plotting data that was not scaled. </param>
        private void ChartAmplitudeSpectrum( double[] frequencies, double[] magnitudes, double scaleFactor )
        {
            // adjust abscissa scale.
            double frequencyAxisMin = frequencies[frequencies.GetLowerBound( 0 )];
            double frequencyAxisMax = frequencies[frequencies.GetUpperBound( 0 )];

            // adjust ordinate scale.
            double amplitudeSpectrumAxisMax = 1d;
            double amplitudeSpectrumAxisMin = 0d;

            if ( this._SpectrumChart.ChartArea.AxisX.Minimum != frequencyAxisMin
                || this._SpectrumChart.ChartArea.AxisX.Maximum != frequencyAxisMax
                || this._SpectrumChart.ChartArea.AxisY.Minimum != amplitudeSpectrumAxisMin
                || this._SpectrumChart.ChartArea.AxisY.Maximum != amplitudeSpectrumAxisMax )
                this.CreateSpectrumChart( frequencyAxisMin, amplitudeSpectrumAxisMin, frequencyAxisMax, amplitudeSpectrumAxisMax );

            // scale the magnitudes
            var amplitudes = new double[frequencies.Length];
            for ( int i = frequencies.GetLowerBound( 0 ), loopTo = frequencies.GetUpperBound( 0 ); i <= loopTo; i++ )
            {
                amplitudes[i] = scaleFactor * magnitudes[i];
            }

            _ = this._SpectrumChart.GraphLineSeries( this._SpectrumSeries.Name, System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine, frequencies, magnitudes );
            // this._SpectrumChartPanel.Invalidate();
        }

#endregion

        #region " SIGNAL CHART "

        private isr.Visuals.Charting.LineChartControl _SignalChart;

        private System.Windows.Forms.DataVisualization.Charting.Series _SignalSeries;

        /// <summary>   Creates the Scope for display of voltages during the epoch. </summary>
        /// <remarks>   David, 2020-10-26. </remarks>
        /// <param name="abscissaMin">  The abscissa minimum. </param>
        /// <param name="ordinateMin">  The ordinate minimum. </param>
        /// <param name="abscissaMax">  The abscissa maximum. </param>
        /// <param name="ordinateMax">  The ordinate maximum. </param>
        private void CreateSignalChart( double abscissaMin, double ordinateMin, double abscissaMax, double ordinateMax )
        {
            if ( this._SignalSeries is object )
            {
                _ = this._SignalChart.Series.Remove( this._SignalSeries );
                this._SignalSeries.Dispose();
            }

            if ( this._SignalChart is not object )
            {
                this._SignalChart = new();
                this._SignalChartPanel.Controls.Add( this._SignalChart );
                this._SignalChart.Dock = DockStyle.Fill;
                this._SignalChart.InitializeKnownState( false );
            }

            this._SignalSeries = this._SignalChart.SetupLineChartChart( "signal",  abscissaMin, ordinateMin,
                                                                        abscissaMax, ordinateMax,
                                                                        "Time, Seconds", "Volts" );
            // this._SignalChart.AddTitle( "Voltage versus Time" );
            this._SignalChart.ToggleDashedZoomRectangleMouseHandlers( true );
            this._SignalChart.PopulateContextMenu();
            this._SignalChart.ToggleContextMenu( true );
        }

        /// <summary>
        /// Plot the DP signal from the entire epoch data simulating a strip chart effect.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="times">      The times. </param>
        /// <param name="amplitudes"> The amplitudes. </param>
        private void ChartSignal( float[] times, float[] amplitudes )
        {

            // plot all but last, which was plotted above
            var newApms = new double[times.Length];
            var newTimes = new double[times.Length];
            for ( int i = times.GetLowerBound( 0 ), loopTo = times.GetUpperBound( 0 ); i <= loopTo; i++ )
            {
                newApms[i] = amplitudes[i];
                newTimes[i] = times[i];
            }
            this.ChartSignal( newTimes, newApms );
        }

        /// <summary>
        /// Plot the DP signal from the entire epoch data simulating a strip chart effect.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="times">      The times. </param>
        /// <param name="amplitudes"> The amplitudes. </param>
        private void ChartSignal( double[] times, double[] amplitudes )
        {
            // adjust abscissa scale.
            double timeAxisMin = times[times.GetLowerBound( 0 )];
            double timeAxisMax = times[times.GetUpperBound( 0 )];

            // adjust ordinate scale.
            double amplitudeAxisMax = 1d;
            double amplitudeAxisMin = -1d;

            if ( this._SignalChart.ChartArea.AxisX.Minimum != timeAxisMin
                || this._SignalChart.ChartArea.AxisX.Maximum != timeAxisMax
                || this._SignalChart.ChartArea.AxisY.Minimum != amplitudeAxisMin
                || this._SignalChart.ChartArea.AxisY.Maximum != amplitudeAxisMax )
                this.CreateSignalChart( timeAxisMin, amplitudeAxisMin, timeAxisMax, amplitudeAxisMax );

            _ = this._SignalChart.GraphLineSeries( this._SignalSeries.Name, System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine, times, amplitudes );
            // this._SignalChartPanel.Invalidate();
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Event handler. Called by _SignalComboBox for validated events. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void SignalComboBox_Validated( object sender, EventArgs e )
        {
            this.UpdateSignal();
        }

        /// <summary> Event handler. Called by _cyclesTextBox for validating events. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void CyclesTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorProvider.SetError( this._CyclesTextBox, string.Empty );
            if ( int.TryParse( this._CyclesTextBox.Text, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out int value ) )
            {
                if ( value < 1 )
                {
                    e.Cancel = true;
                    this._ErrorProvider.SetError( this._CyclesTextBox, "Must exceed 1" );
                }
                else
                {
                    this.UpdateSignal();
                }
            }
            else
            {
                e.Cancel = true;
                this._ErrorProvider.SetError( this._CyclesTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Event handler. Called by _PhaseTextBox for validating events. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void PhaseTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorProvider.SetError( this._PhaseTextBox, string.Empty );
            if ( int.TryParse( this._PhaseTextBox.Text, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out _ ) )
            {
                this.UpdateSignal();
            }
            else
            {
                e.Cancel = true;
                this._ErrorProvider.SetError( this._CyclesTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Event handler. Called by _pointsTextBox for validating events. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void PointsTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorProvider.SetError( this._PointsTextBox, string.Empty );
            this._ErrorProvider.SetError( this._PhaseTextBox, string.Empty );
            if ( int.TryParse( this._PointsTextBox.Text, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out int value ) )
            {
                if ( value < 2 )
                {
                    e.Cancel = true;
                    this._ErrorProvider.SetError( this._PointsTextBox, "Must exceed 2" );
                }
                else
                {
                    // Set initial values defining the signal
                    if ( int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) < int.Parse( this._PointsToDisplayTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) )
                    {
                        this._PointsToDisplayTextBox.Text = this._PointsTextBox.Text;
                    }

                    this.UpdateSignal();
                }
            }
            else
            {
                e.Cancel = true;
                this._ErrorProvider.SetError( this._CyclesTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Event handler. Called by _pointsToDisplayTextBox for validating events. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void PointsToDisplayTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorProvider.SetError( this._PointsToDisplayTextBox, string.Empty );
            if ( int.TryParse( this._PointsToDisplayTextBox.Text, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out int value ) )
            {
                if ( value < 2 )
                {
                    e.Cancel = true;
                    this._ErrorProvider.SetError( this._PointsToDisplayTextBox, "Must exceed 2" );
                }
                // Set initial values defining the signal
                else if ( int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) < int.Parse( this._PointsToDisplayTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) )
                {
                    this._PointsToDisplayTextBox.Text = this._PointsTextBox.Text;
                }
            }
            else
            {
                e.Cancel = true;
                this._ErrorProvider.SetError( this._PointsToDisplayTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Event handler. Called by _startStopCheckBox for checked changed events. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void StartStopCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this._StartStopCheckBox.Text = this._StartStopCheckBox.Checked ? "&Stop" : "&Start";
            if ( this._StartStopCheckBox.Enabled && this._StartStopCheckBox.Checked )
            {
                var algorithm = this.SelectedExample;
                switch ( algorithm )
                {
                    case Example.DiscreteFourierTransform:
                    case Example.MixedRadixFFT:
                    case Example.WisdomFFT:
                        {
                            this._CountToolStripStatusLabel.Text = "0";
                            do
                            {
                                if ( this._DoubleRadioButton.Checked )
                                {
                                    this.CalculateSpectrumDouble( algorithm );
                                }
                                else
                                {
                                    this.CalculateSpectrumSingle( algorithm );
                                }

                                Application.DoEvents();
                                int count = int.Parse( this._CountToolStripStatusLabel.Text, System.Globalization.CultureInfo.CurrentCulture ) + 1;
                                this._CountToolStripStatusLabel.Text = count.ToString( System.Globalization.CultureInfo.CurrentCulture );
                            }
                            while ( this._StartStopCheckBox.Checked & false );
                            break;
                        }

                    case Example.SlidingFFT:
                        {
                            if ( this._DoubleRadioButton.Checked )
                            {
                                this.SlidingFft( 0.5d );
                            }
                            else
                            {
                                this.SlidingFft( 0.5f );
                            }

                            break;
                        }

                    default:
                        {
                            this.ShowStatus( $"Unknown algorithm: {algorithm}::{algorithm.Description()}" );
                            break;
                        }
                }

                if ( this._StartStopCheckBox.Checked )
                {
                    this._StartStopCheckBox.Enabled = false;
                    this._StartStopCheckBox.Checked = false;
                    this._StartStopCheckBox.Enabled = true;
                }
            }
            else
            {
                this._CountToolStripStatusLabel.Text = "0";
            }
        }

#endregion

    }
}
