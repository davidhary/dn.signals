using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using FastEnums;

using isr.Signals.Base;

namespace isr.Signals.Demo
{

    /// <summary>
    /// Includes test program for calculating the spectrum using DFT, FFTW, and sliding Fourier
    /// transforms.
    /// </summary>
    /// <remarks>
    /// David, 2005-10-11, 1.0.2110. Convert to .NET <para>
    /// Launch this form by calling its Show or ShowDialog method from its default instance.
    /// </para><para>
    /// (c) 2005 Integrated Scientific Resources, Inc.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public partial class WisdomSpectrumView : UserControl
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public WisdomSpectrumView() : base()
        {
            // Initialize user components that might be affected by resize or paint actions

            // This method is required by the Windows Form Designer.
            this.InitializeComponent();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this._SignalSeries?.Dispose();
                    this._SignalSeries = null;
                    this._SignalChart?.Dispose();
                    this._SignalChart = null;
                    this._SpectrumSeries?.Dispose();
                    this._SpectrumSeries = null;
                    this._SpectrumChart?.Dispose();
                    this._SpectrumChart = null;
                    this.components?.Dispose();
                }
            }
            // Free shared unmanaged resources

            finally
            {
                // Invoke the base class dispose method
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the selected example. </summary>
        /// <value> The selected example. </value>
        private Example SelectedExample => ( Example ) (( KeyValuePair<Enum, string> ) this._ExampleComboBox.SelectedItem).Key;

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Occurs when the form is loaded. </summary>
        /// <remarks>
        /// Use this method for doing any final initialization right before the form is shown.  This is a
        /// good place to change the Visible and ShowInTaskbar properties to start the form as hidden.
        /// Starting a form as hidden is useful for forms that need to be running but that should not
        /// show themselves right away, such as forms with a notify icon in the task bar.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      <see cref="System.EventArgs"/> </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;

                // Initialize and set the user interface
                this.InitializeUserInterface();
            }
            catch
            {

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " USER INTERFACE "

        /// <summary> Shows the status. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="message"> The message. </param>
        private void ShowStatus( string message )
        {
            this._NotesMessageList.AppendText( message );
            this._StatusStatusBarPanel.Text = message;
        }

        /// <summary>Gets or sets the data format</summary>
        private const string _ListFormat = "{0:0.000000000000000}{1}{2:0.000000000000000}";

        /// <summary> Enumerates the action options. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private enum Example
        {

            /// <summary> An enum constant representing the discrete fourier transform option. </summary>
            [System.ComponentModel.Description( "Discrete Fourier Transform" )]
            DiscreteFourierTransform,

            /// <summary> An enum constant representing the sliding FFT option. </summary>
            [System.ComponentModel.Description( "Sliding FFT" )]
            SlidingFFT,

            /// <summary> An enum constant representing the wisdom FFT option. </summary>
            [System.ComponentModel.Description( "Wisdom FFT" )]
            WisdomFFT
        }

        /// <summary> Initializes the user interface and tool tips. </summary>
        /// <remarks> Call this method from the form load method to set the user interface. </remarks>
        private void InitializeUserInterface()
        {

            // tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
            // tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")

            // Set initial values defining the signal
            this._PointsTextBox.Text = "16"; // "1000"
            this._CyclesTextBox.Text = "1"; // "11.5"
            this._PhaseTextBox.Text = "0"; // "45"
            this._PointsToDisplayTextBox.Text = "100";
            this._SignalDurationTextBox.Text = "1";

            // set the default sample
            this.PopulateExampleComboBox();
            this._ExampleComboBox.SelectedIndex = 0;

            // create the two charts.
            this.CreateSpectrumChart( 1, 0, 100, 1 );
            this.CreateSignalChart( 0, -1, 1, 1 );

            // plot the default sign wave
            _ = this.SineWaveDouble();
        }

        #endregion

        #region " SIGNALS "

        /// <summary> Calculates the sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequency"> . </param>
        /// <param name="phase">     . </param>
        /// <param name="elements">  . </param>
        /// <returns> A Double() </returns>
        private float[] SineWave( float frequency, float phase, int elements )
        {

            // get the signal
            var signal = isr.Signals.Waveform.Signal.Sine( frequency, phase, elements );

            // Plot the Signal 
            this.ChartSignal( isr.Signals.Waveform.Signal.Ramp( float.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / elements, elements ), signal );
            return signal;
        }

        /// <summary> Calculates the sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequency"> . </param>
        /// <param name="phase">     . </param>
        /// <param name="elements">  . </param>
        /// <returns> A Double() </returns>
        private double[] SineWave( double frequency, double phase, int elements )
        {

            // get the signal
            var signal = isr.Signals.Waveform.Signal.Sine( frequency, phase, elements );

            // Plot the Signal 
            this.ChartSignal( isr.Signals.Waveform.Signal.Ramp( double.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / elements, elements ), signal );
            return signal;
        }

        /// <summary> Calculates the sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A Double() </returns>
        private double[] SineWaveDouble()
        {

            // Read number of points from the text box.
            int signalPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            double signalCycles = double.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
            double signalPhase = isr.Signals.Waveform.Signal.ToRadians( double.Parse( this._PhaseTextBox.Text, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture ) );
            return this.SineWave( signalCycles, signalPhase, signalPoints );
        }

        /// <summary> Calculates the sine wave. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <returns> A Single() </returns>
        private float[] SineWaveSingle()
        {

            // Read number of points from the text box.
            int signalPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            float signalCycles = float.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );
            float signalPhase = Convert.ToSingle( isr.Signals.Waveform.Signal.ToRadians( float.Parse( this._PhaseTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) ) );
            return this.SineWave( signalCycles, signalPhase, signalPoints );
        }

        /// <summary> The copy signal format. </summary>
        private const string _CopySigFormat = "    Copy Signal:  {0:0.###} ms ";

        /// <summary> The FFT initialize format. </summary>
        private const string _FftInitFormat = " FFT Initialize:  {0:0} ms ";

        /// <summary> The FFT calculate format. </summary>
        private const string _FftCalcFormat = "  FFT Calculate:  {0:0.###} ms ";

        /// <summary> The frq calculate format. </summary>
        private const string _FrqCalcFormat = "FFT Frequencies:  {0:0.###} ms ";

        /// <summary> The magnitude calculate format. </summary>
        private const string _MagCalcFormat = "  FFT Magnitude:  {0:0.###} ms ";

        /// <summary> The inverse calculate format. </summary>
        private const string _InvCalcFormat = "    Inverse FFT:  {0:0.###} ms ";

        /// <summary> The charting format. </summary>
        private const string _ChartingFormat = "  Charting time:  {0:0} ms ";

        #endregion

        #region " SPECTRUM: DOUBLE "

        /// <summary>
        /// Calculates the Spectrum based on the selected algorithm and displays the results.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="algorithm"> The algorithm. </param>
        private void CalculateSpectrumDouble( Example algorithm )
        {
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            TimeSpan duration;
            var timingBuilder = new System.Text.StringBuilder();
            _ = timingBuilder.AppendLine( algorithm.Description() );
            this.ShowStatus( $"Calculating double-precision {algorithm.Description()} FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create and plot the sine wave.
            var signal = this.SineWaveDouble();

            // Allocate array for the Real part of the DFT.
            double[] real;
            real = new double[fftPoints];

            // Allocate array for the imaginary part of the DFT.
            double[] imaginary;
            imaginary = new double[fftPoints];

            // ------------ SELECT PROPERTIES ---------------

            // select the spectrum type
            isr.Signals.Spectrum.Spectrum spectrum;
            FourierTransformBase fft;
            switch ( algorithm )
            {
                case Example.DiscreteFourierTransform:
                    {
                        fft = new isr.Signals.Fourier.DiscreteFourierTransform();
                        break;
                    }

                case Example.WisdomFFT:
                    {
                        fft = new Wisdom.Dft();
                        break;
                    }

                default:
                    {
                        return;
                    }
            }

            // scale the FFT by the Window power and data points
            // Remove mean before calculating the fft
            spectrum = new isr.Signals.Spectrum.Spectrum( fft ) {
                IsScaleFft = true,
                IsRemoveMean = this._RemoveMeanCheckBox.Checked
            };

            // Use Taper Window as selected
            spectrum.TaperWindow = this._TaperWindowCheckBox.Checked ? new isr.Signals.TaperWindow.BlackmanTaperWindow() : null;

            // Initialize the FFT.
            timeKeeper.Start();
            spectrum.Initialize( real, imaginary );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _FftInitFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // ------------ CREATE TEH SIGNAL ---------------

            timeKeeper.Start();
            signal.CopyTo( real, 0 );
            Array.Clear( imaginary, 0, imaginary.Length );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _CopySigFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // ------------ Calculate Forward and Inverse FFT ---------------

            // Compute the FFT.
            timeKeeper.Start();
            spectrum.Calculate( real, imaginary );
            duration = timeKeeper.Elapsed;

            // Display time
            this._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms";
            _ = timingBuilder.AppendFormat( _FftCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // ------- Calculate FFT outcomes ---------

            // Get the FFT magnitudes
            double[] magnitudes;
            _ = real.Magnitudes( imaginary );
            timeKeeper.Start();
            magnitudes = real.Magnitudes( imaginary );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _MagCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();


            // Get the Frequency
            double[] frequencies;
            timeKeeper.Start();
            frequencies = isr.Signals.Waveform.Signal.Frequencies( 1.0d, fftPoints );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _FrqCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // Compute the inverse transform.
            timeKeeper.Start();
            fft.Inverse( real, imaginary );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _InvCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // Display the forward and inverse data.
            this.DisplayCalculationAccuracy( signal, real );

            // ------------ Plot FFT outcome ---------------
            timeKeeper.Start();
            this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1d );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _ChartingFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();
            this._TimingTextBox.Text = timingBuilder.ToString();
        }

        /// <summary>
        /// Calculates the Spectrum based on the selected algorithm and displays the results.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="algorithm"> The algorithm. </param>
        private void CalculateSpectrumSingle( Example algorithm )
        {
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            TimeSpan duration;
            var timingBuilder = new System.Text.StringBuilder();
            _ = timingBuilder.AppendLine( algorithm.Description() );
            this.ShowStatus( $"Calculating single-precision {algorithm.Description()} FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create and plot the sine wave.
            var signal = this.SineWaveSingle();

            // Allocate array for the Real part of the DFT.
            float[] real;
            real = new float[fftPoints];

            // Allocate array for the imaginary part of the DFT.
            float[] imaginary;
            imaginary = new float[fftPoints];

            // ------------ SELECT PROPERTIES ---------------

            // select the spectrum type
            isr.Signals.Spectrum.Spectrum spectrum;
            FourierTransformBase fft;
            switch ( algorithm )
            {
                case Example.DiscreteFourierTransform:
                    {
                        fft = new isr.Signals.Fourier.DiscreteFourierTransform();
                        break;
                    }

                case Example.WisdomFFT:
                    {
                        fft = new Wisdom.Dft();
                        break;
                    }

                default:
                    {
                        return;
                    }
            }

            // scale the FFT by the Window power and data points
            // Remove mean before calculating the fft
            spectrum = new isr.Signals.Spectrum.Spectrum( fft ) {
                IsScaleFft = true,
                IsRemoveMean = this._RemoveMeanCheckBox.Checked
            };

            // Use Taper Window as selected
            spectrum.TaperWindow = this._TaperWindowCheckBox.Checked ? new isr.Signals.TaperWindow.BlackmanTaperWindow() : null;

            // Initialize the FFT.
            timeKeeper.Start();
            spectrum.Initialize( real, imaginary );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _FftInitFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // ------------ CREATE TEH SIGNAL ---------------

            timeKeeper.Start();
            signal.CopyTo( real, 0 );
            Array.Clear( imaginary, 0, imaginary.Length );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _CopySigFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // ------------ Calculate Forward and Inverse FFT ---------------

            // Compute the FFT.
            timeKeeper.Start();
            spectrum.Calculate( real, imaginary );
            duration = timeKeeper.Elapsed;

            // Display time
            this._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms";
            _ = timingBuilder.AppendFormat( _FftCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // ------- Calculate FFT outcomes ---------

            // Get the FFT magnitudes
            float[] magnitudes;
            _ = real.Magnitudes( imaginary );
            timeKeeper.Start();
            magnitudes = real.Magnitudes( imaginary );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _MagCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();


            // Get the Frequency
            float[] frequencies;
            timeKeeper.Start();
            frequencies = isr.Signals.Waveform.Signal.Frequencies( 1.0f, fftPoints );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _FrqCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // Compute the inverse transform.
            timeKeeper.Start();
            fft.Inverse( real, imaginary );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _InvCalcFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();

            // Display the forward and inverse data.
            this.DisplayCalculationAccuracy( signal, real );

            // ------------ Plot FFT outcome ---------------
            timeKeeper.Start();
            this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1f );
            duration = timeKeeper.Elapsed;
            _ = timingBuilder.AppendFormat( _ChartingFormat, duration.TotalMilliseconds );
            _ = timingBuilder.AppendLine();
            this._TimingTextBox.Text = timingBuilder.ToString();
        }

        /// <summary> Displays the test results. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="signal"> . </param>
        /// <param name="real">   . </param>
        private void DisplayCalculationAccuracy( double[] signal, double[] real )
        {
            if ( signal is null )
            {
                return;
            }

            if ( real is null )
            {
                return;
            }

            int elementCount = Math.Min( real.Length, int.Parse( this._PointsToDisplayTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
            if ( elementCount <= 0 )
            {
                return;
            }

            // Display the forward and inverse data.
            double totalError = 0d;
            this._FftListBox.Items.Clear();
            double value;
            for ( int i = 0, loopTo = elementCount - 1; i <= loopTo; i++ )
            {
                value = signal[i] - real[i];
                totalError += value * value;
                _ = this._FftListBox.Items.Add( string.Format( System.Globalization.CultureInfo.CurrentCulture, _ListFormat, signal[i], "\t", real[i] ) );
            }

            totalError = Math.Sqrt( totalError / Convert.ToDouble( elementCount ) );
            this._ErrorStatusBarPanel.Text = $"{totalError:0.###E+0}";
            this._MainStatusBar.Invalidate();
        }

        /// <summary> Displays the test results. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="signal"> . </param>
        /// <param name="real">   . </param>
        private void DisplayCalculationAccuracy( float[] signal, float[] real )
        {
            if ( signal is null )
            {
                return;
            }

            if ( real is null )
            {
                return;
            }

            int elementCount = Math.Min( real.Length, int.Parse( this._PointsToDisplayTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
            if ( elementCount <= 0 )
            {
                return;
            }

            // Display the forward and inverse data.
            double totalError = 0d;
            this._FftListBox.Items.Clear();
            double value;
            for ( int i = 0, loopTo = elementCount - 1; i <= loopTo; i++ )
            {
                value = signal[i] - real[i];
                totalError += value * value;
                _ = this._FftListBox.Items.Add( string.Format( System.Globalization.CultureInfo.CurrentCulture, _ListFormat, signal[i], "\t", real[i] ) );
            }

            totalError = Math.Sqrt( totalError / Convert.ToDouble( elementCount ) );
            this._ErrorStatusBarPanel.Text = $"{totalError:0.###E+0}";
            this._MainStatusBar.Invalidate();
        }

        /// <summary> Populates the list of options in the action combo box. </summary>
        /// <remarks>
        /// David, 2004-11-08. Created <para>
        /// It seems that out enumerated list does not work very well with this list. </para>
        /// </remarks>
        private void PopulateExampleComboBox()
        {

            // set the action list
            this._ExampleComboBox.Items.Clear();
            this._ExampleComboBox.DataSource = typeof( Example ).ValueDescriptionPairs().ToList();
            this._ExampleComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._ExampleComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
        }

        /// <summary> Evaluates sliding FFT until stopped. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
        /// observing the changes in the spectrum. </param>
        private void SlidingFft( double noiseFigure )
        {
            this.ShowStatus( "Calculating double-precision sliding FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            var signal = this.SineWaveDouble();

            // Allocate array for the Real part of the DFT.
            double[] real;
            real = new double[fftPoints];
            signal.CopyTo( real, 0 );

            // Allocate array for the imaginary part of the DFT.
            double[] imaginary;
            imaginary = new double[fftPoints];

            // ------------ Calculate Forward and Inverse FFT ---------------

            // Create a new instance of the FFTW class
            var fft = new Wisdom.Dft();

            // Compute the FFT.
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            timeKeeper.Start();
            fft.Forward( real, imaginary );
            var duration = timeKeeper.Elapsed;

            // Display time
            this._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms";

            // Clear the data
            this._FftListBox.Items.Clear();

            // Clear the error value
            this._ErrorStatusBarPanel.Text = string.Empty;

            // Gets the FFT magnitudes
            double[] magnitudes;

            // Gets the Frequency
            double[] frequencies;

            // Initialize the last point of the signal to the last point.
            int lastSignalPoint = fftPoints - 1;

            // Initialize the first point of the signal to the last point.
            int firstSignalPoint = 0;

            // Calculate the phase of the last point of the sine wave
            double deltaPhase = 2d * Math.PI * float.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / Convert.ToSingle( fftPoints );

            // get the signal phase of the last point
            double signalPhase = isr.Signals.Waveform.Signal.ToRadians( double.Parse( this._PhaseTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) );
            signalPhase += deltaPhase * lastSignalPoint;

            // Use a new frequency for the sine wave to see how
            // the old is phased out and the new gets in
            deltaPhase *= 2d;

            // First element in the previous Real part of the time series
            double oldReal;

            // First element in the previous imaginary part of the time series.
            double oldImaginary;

            // New Real part of Signal
            double newReal;

            // New imaginary part of Signal
            double newImaginary;

            // Create a new instance of the sliding fft class
            var slidingFft = new isr.Signals.Fourier.SlidingFourierTransform();

            // Initialize sliding FFT coefficients.
            slidingFft.Forward( real, imaginary );

            Random rnd = new( DateTime.Now.Second );
            // Recalculate the sliding FFT
            while ( this._StartStopCheckBox.Checked )
            {

                // Allow other events to occur
                Application.DoEvents();

                // ------- Calculate FFT outcomes ---------

                // Get the FFT magnitudes
                magnitudes = real.Magnitudes( imaginary );

                // Get the Frequency
                frequencies = isr.Signals.Waveform.Signal.Frequencies( 1.0d, fftPoints );

                // ------------ Plot the Signal ---------------

                this.ChartSignal( isr.Signals.Waveform.Signal.Ramp( double.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / fftPoints, fftPoints ), signal );

                // ------------ Plot FFT outcome ---------------

                this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1d );

                // Update the previous values of the signal
                oldReal = signal[firstSignalPoint];
                oldImaginary = 0d;

                // Get new signal values.
                signalPhase += deltaPhase;
                newReal = Math.Sin( signalPhase );
                newImaginary = 0d;

                // Add some random noise to make it interesting.
                newReal += noiseFigure * 2d * (rnd.NextDouble() - 0.5d);

                // Update the signal itself.

                // Update the last point.
                lastSignalPoint += 1;
                if ( lastSignalPoint >= fftPoints )
                {
                    lastSignalPoint = 0;
                }

                // Update the first point.
                firstSignalPoint += 1;
                if ( firstSignalPoint >= fftPoints )
                {
                    firstSignalPoint = 0;
                }

                // Place new data in the signal itself.
                signal[lastSignalPoint] = newReal;

                // Calculate the sliding FFT coefficients.
                _ = slidingFft.Update( newReal, newImaginary, oldReal, oldImaginary );
            }
        }

        /// <summary> Evaluates sliding FFT until stopped. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="noiseFigure"> Specifies the relative noise level to add to the signal for
        /// observing the changes in the spectrum. </param>
        private void SlidingFft( float noiseFigure )
        {
            this.ShowStatus( "Calculating single-precision sliding FFT" );

            // ------------ Create the Signal for FFT ---------------

            // Set FFT points 
            int fftPoints = int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture );

            // Create cycles of the sine wave.
            var signal = this.SineWaveSingle();

            // Allocate array for the Real part of the DFT.
            float[] real;
            real = new float[fftPoints];
            signal.CopyTo( real, 0 );

            // Allocate array for the imaginary part of the DFT.
            float[] imaginary;
            imaginary = new float[fftPoints];

            // ------------ Calculate Forward and Inverse FFT ---------------

            // Create a new instance of the FFTW class
            var fft = new Wisdom.Dft();

            // Compute the FFT.
            Stopwatch timeKeeper;
            timeKeeper = new Stopwatch();
            timeKeeper.Start();
            fft.Forward( real, imaginary );
            var duration = timeKeeper.Elapsed;

            // Display time
            this._FftTimeStatusBarPanel.Text = $"{duration.TotalMilliseconds:0.###} ms";

            // Clear the data
            this._FftListBox.Items.Clear();
            this._ErrorStatusBarPanel.Text = string.Empty;

            // Gets the FFT magnitudes
            float[] magnitudes;

            // Gets the Frequency
            float[] frequencies;

            // Initialize the last point of the signal to the last point.
            int lastSignalPoint = fftPoints - 1;

            // Initialize the first point of the signal to the last point.
            int firstSignalPoint = 0;

            // Calculate the phase of the last point of the sine wave
            float deltaPhase = 2.0f * Convert.ToSingle( Math.PI ) * float.Parse( this._CyclesTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / fftPoints;

            // get the signal phase of the last point
            float signalPhase = Convert.ToSingle( isr.Signals.Waveform.Signal.ToRadians( float.Parse( this._PhaseTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) ) );
            signalPhase += deltaPhase * lastSignalPoint;

            // Use a new frequency for the sine wave to see how
            // the old is phased out and the new gets in
            deltaPhase *= 2f;
            float oldReal; // First element in the previous
                           // real part of the time series.

            float oldImaginary; // First element in the previous
                                // imaginary time series.

            float newReal; // New Real part of Signal
            float newImaginary; // New imaginary part of Signal

            // Create a new instance of the sliding fft class
            var slidingFft = new isr.Signals.Fourier.SlidingFourierTransform();

            // Initialize sliding FFT coefficients.
            slidingFft.Forward( real, imaginary );

            Random rnd = new( DateTime.Now.Second );
            // Recalculate the sliding FFT
            while ( this._StartStopCheckBox.Checked )
            {

                // Allow other events to occur
                Application.DoEvents();

                // ------- Calculate FFT outcomes ---------

                // Get the FFT magnitudes
                magnitudes = real.Magnitudes( imaginary );

                // Get the Frequency
                frequencies = isr.Signals.Waveform.Signal.Frequencies( 1.0f, fftPoints );

                // ------------ Plot the Signal ---------------

                this.ChartSignal( isr.Signals.Waveform.Signal.Ramp( float.Parse( this._SignalDurationTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) / fftPoints, fftPoints ), signal );

                // ------------ Plot FFT outcome ---------------

                this.ChartAmplitudeSpectrum( frequencies, magnitudes, 1f );

                // Update the previous values of the signal
                oldReal = signal[firstSignalPoint];
                oldImaginary = 0f;

                // Get new signal values.
                signalPhase += deltaPhase;
                newReal = Convert.ToSingle( Math.Sin( signalPhase ) );
                newImaginary = 0f;

                // Add some random noise to make it interesting.
                newReal += noiseFigure * 2.0f * ( float ) (rnd.NextDouble() - 0.5f);

                // Update the last point.
                lastSignalPoint += 1;
                if ( lastSignalPoint >= fftPoints )
                {
                    lastSignalPoint = 0;
                }

                // Update the first point.
                firstSignalPoint += 1;
                if ( firstSignalPoint >= fftPoints )
                {
                    firstSignalPoint = 0;
                }

                // Place new data in the signal itself.
                signal[lastSignalPoint] = newReal;

                // Calculate the sliding FFT coefficients.
                _ = slidingFft.Update( newReal, newImaginary, oldReal, oldImaginary );
            }
        }

        #endregion

        #region " AMPLITUDE SPECTRUM CHART "

        private isr.Visuals.Charting.LineChartControl _SpectrumChart;

        private System.Windows.Forms.DataVisualization.Charting.Series _SpectrumSeries;

        /// <summary> Creates the chart for displaying the amplitude spectrum. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        private void CreateSpectrumChart( double abscissaMin, double ordinateMin, double abscissaMax, double ordinateMax )
        {
            if ( this._SpectrumSeries is object )
            {
                _ = this._SpectrumChart.Series.Remove( this._SpectrumSeries );
                this._SpectrumSeries.Dispose();
            }

            if ( this._SpectrumChart is not object )
            {
                this._SpectrumChart = new();
                this._SpectrumChartPanel.Controls.Add( this._SpectrumChart );
                this._SpectrumChart.Dock = DockStyle.Fill;
                this._SpectrumChart.InitializeKnownState( false );
            }

            this._SpectrumSeries = this._SpectrumChart.SetupLineChartChart( "spectrum", abscissaMin, ordinateMin,
                                                                            abscissaMax, ordinateMax,
                                                                            "Frequency, Hz", "Amplitude, Volts" );
            this._SpectrumChart.AddTitle( "Spectrum" );
            this._SpectrumChart.ToggleDashedZoomRectangleMouseHandlers( true );
            this._SpectrumChart.PopulateContextMenu();
            this._SpectrumChart.ToggleContextMenu( true );
        }

        /// <summary>
        /// Plots the amplitude spectrum using the spectrum amplitudes and frequencies.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequencies"> Holds the spectrum frequencies. </param>
        /// <param name="magnitudes">  Holds the spectrum amplitudes. </param>
        /// <param name="scaleFactor"> Specifies the scale factor by which to scale the spectrum for
        /// plotting data that was not scaled. </param>
        private void ChartAmplitudeSpectrum( float[] frequencies, float[] magnitudes, float scaleFactor )
        {

            // copy the amplitudes to a double array
            var amplitudes = new double[frequencies.Length];
            magnitudes.CopyTo( amplitudes, 0 );

            // copy the frequencies to a double array.
            var freq = new double[frequencies.Length];
            frequencies.CopyTo( freq, 0 );
            this.ChartAmplitudeSpectrum( freq, amplitudes, scaleFactor );
        }

        /// <summary>
        /// Plots the amplitude spectrum using the spectrum amplitudes and frequencies.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="frequencies"> Holds the spectrum frequencies. </param>
        /// <param name="magnitudes">  Holds the spectrum amplitudes. </param>
        /// <param name="scaleFactor"> Specifies the scale factor by which to scale the spectrum for
        /// plotting data that was not scaled. </param>
        private void ChartAmplitudeSpectrum( double[] frequencies, double[] magnitudes, double scaleFactor )
        {
            // adjust abscissa scale.
            double frequencyAxisMin = frequencies[frequencies.GetLowerBound( 0 )];
            double frequencyAxisMax = frequencies[frequencies.GetUpperBound( 0 )];

            // adjust ordinate scale.
            double amplitudeSpectrumAxisMax = 1d;
            double amplitudeSpectrumAxisMin = 0d;

            if ( this._SpectrumChart.ChartArea.AxisX.Minimum != frequencyAxisMin
                || this._SpectrumChart.ChartArea.AxisX.Maximum != frequencyAxisMax
                || this._SpectrumChart.ChartArea.AxisY.Minimum != amplitudeSpectrumAxisMin
                || this._SpectrumChart.ChartArea.AxisY.Maximum != amplitudeSpectrumAxisMax )
                this.CreateSpectrumChart( frequencyAxisMin, amplitudeSpectrumAxisMin, frequencyAxisMax, amplitudeSpectrumAxisMax );

            // scale the magnitudes
            var amplitudes = new double[frequencies.Length];
            for ( int i = frequencies.GetLowerBound( 0 ), loopTo = frequencies.GetUpperBound( 0 ); i <= loopTo; i++ )
            {
                amplitudes[i] = scaleFactor * magnitudes[i];
            }
            _ = this._SpectrumChart.GraphLineSeries( this._SpectrumSeries.Name, System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine, frequencies, magnitudes );
            // this._SpectrumChartPanel.Invalidate();
        }

        #endregion

        #region " SIGNAL CHART "

        private isr.Visuals.Charting.LineChartControl _SignalChart;

        private System.Windows.Forms.DataVisualization.Charting.Series _SignalSeries;

        /// <summary>   Creates the Scope for display of voltages during the epoch. </summary>
        /// <remarks>   David, 2020-10-26. </remarks>
        /// <param name="abscissaMin">  The abscissa minimum. </param>
        /// <param name="ordinateMin">  The ordinate minimum. </param>
        /// <param name="abscissaMax">  The abscissa maximum. </param>
        /// <param name="ordinateMax">  The ordinate maximum. </param>
        private void CreateSignalChart( double abscissaMin, double ordinateMin, double abscissaMax, double ordinateMax )
        {
            if ( this._SignalSeries is object )
            {
                _ = this._SignalChart.Series.Remove( this._SignalSeries );
                this._SignalSeries.Dispose();
            }

            if ( this._SignalChart is not object )
            {
                this._SignalChart = new();
                this._SignalChartPanel.Controls.Add( this._SignalChart );
                this._SignalChart.Dock = DockStyle.Fill;
                this._SignalChart.InitializeKnownState( false );
            }

            this._SignalSeries = this._SignalChart.SetupLineChartChart( "signal", abscissaMin, ordinateMin,
                                                                        abscissaMax, ordinateMax,
                                                                        "Time, Seconds", "Volts" );
            this._SignalChart.AddTitle( "Voltage versus Time" );
            this._SignalChart.ToggleDashedZoomRectangleMouseHandlers( true );
            this._SignalChart.PopulateContextMenu();
            this._SignalChart.ToggleContextMenu( true );
        }

        /// <summary>
        /// Plot the DP signal from the entire epoch data simulating a strip chart effect.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="times">      The times. </param>
        /// <param name="amplitudes"> The amplitudes. </param>
        private void ChartSignal( float[] times, float[] amplitudes )
        {

            // plot all but last, which was plotted above
            var newApms = new double[times.Length];
            var newTimes = new double[times.Length];
            for ( int i = times.GetLowerBound( 0 ), loopTo = times.GetUpperBound( 0 ); i <= loopTo; i++ )
            {
                newApms[i] = amplitudes[i];
                newTimes[i] = times[i];
            }

            this.ChartSignal( newTimes, newApms );
        }

        /// <summary>
        /// Plot the DP signal from the entire epoch data simulating a strip chart effect.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="times">      The times. </param>
        /// <param name="amplitudes"> The amplitudes. </param>
        private void ChartSignal( double[] times, double[] amplitudes )
        {
            // adjust abscissa scale.
            double timeAxisMin = times[times.GetLowerBound( 0 )];
            double timeAxisMax = times[times.GetUpperBound( 0 )];

            // adjust ordinate scale.
            double amplitudeAxisMax = 1d;
            double amplitudeAxisMin = -1d;

            if ( this._SignalChart.ChartArea.AxisX.Minimum != timeAxisMin
                || this._SignalChart.ChartArea.AxisX.Maximum != timeAxisMax
                || this._SignalChart.ChartArea.AxisY.Minimum != amplitudeAxisMin
                || this._SignalChart.ChartArea.AxisY.Maximum != amplitudeAxisMax )
                this.CreateSignalChart( timeAxisMin, amplitudeAxisMin, timeAxisMax, amplitudeAxisMax );

            _ = this._SignalChart.GraphLineSeries( this._SignalSeries.Name, System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine, times, amplitudes );
            // this._SignalChartPanel.Invalidate();
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Cycles text box validating. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void CyclesTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorErrorProvider.SetError( this._CyclesTextBox, string.Empty );
            if ( int.TryParse( this._CyclesTextBox.Text, out int value ) )
            {
                if ( value < 1 )
                {
                    e.Cancel = true;
                    this._ErrorErrorProvider.SetError( this._CyclesTextBox, "Must exceed 1" );
                }
                else
                {
                    _ = this.SineWaveDouble();
                }
            }
            else
            {
                e.Cancel = true;
                this._ErrorErrorProvider.SetError( this._CyclesTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Phase text box validating. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void PhaseTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorErrorProvider.SetError( this._PhaseTextBox, string.Empty );
            if ( int.TryParse( this._PhaseTextBox.Text, out _ ) )
            {
                _ = this.SineWaveDouble();
            }
            else
            {
                e.Cancel = true;
                this._ErrorErrorProvider.SetError( this._PhaseTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Points text box validating. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void PointsTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorErrorProvider.SetError( this._PointsTextBox, string.Empty );
            if ( int.TryParse( this._PointsTextBox.Text, out int value ) )
            {
                if ( value < 2 )
                {
                    e.Cancel = true;
                    this._ErrorErrorProvider.SetError( this._PointsTextBox, "Must exceed 2" );
                }
                else
                {
                    // Set initial values defining the signal
                    if ( value < int.Parse( this._PointsToDisplayTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) )
                    {
                        this._PointsToDisplayTextBox.Text = this._PointsTextBox.Text;
                    }

                    _ = this.SineWaveDouble();
                }
            }
            else
            {
                e.Cancel = true;
                this._ErrorErrorProvider.SetError( this._PointsTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Points to display text box validating. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Cancel event information. </param>
        private void PointsToDisplayTextBox_Validating( object sender, System.ComponentModel.CancelEventArgs e )
        {
            this._ErrorErrorProvider.SetError( this._PointsToDisplayTextBox, string.Empty );
            if ( int.TryParse( this._PointsToDisplayTextBox.Text, out int value ) )
            {
                if ( value < 2 )
                {
                    e.Cancel = true;
                    this._ErrorErrorProvider.SetError( this._PointsToDisplayTextBox, "Must exceed 2" );
                }
                // Set initial values defining the signal
                else if ( int.Parse( this._PointsTextBox.Text, System.Globalization.CultureInfo.CurrentCulture ) < value )
                {
                    this._PointsToDisplayTextBox.Text = this._PointsTextBox.Text;
                }
            }
            else
            {
                e.Cancel = true;
                this._ErrorErrorProvider.SetError( this._PointsToDisplayTextBox, "Enter numeric value" );
            }
        }

        /// <summary> Starts stop check box checked changed. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        /// <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void StartStopCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this._StartStopCheckBox.Text = this._StartStopCheckBox.Checked ? "&Stop" : "&Start";
            if ( this._StartStopCheckBox.Enabled && this._StartStopCheckBox.Checked )
            {
                var algorithm = this.SelectedExample;
                switch ( algorithm )
                {
                    case Example.DiscreteFourierTransform:
                    case Example.WisdomFFT:
                        {
                            this._CountStatusBarPanel.Text = "0";
                            do
                            {
                                if ( this._DoubleRadioButton.Checked )
                                {
                                    this.CalculateSpectrumDouble( algorithm );
                                }
                                else
                                {
                                    this.CalculateSpectrumSingle( algorithm );
                                }

                                Application.DoEvents();
                                int count = int.Parse( this._CountStatusBarPanel.Text, System.Globalization.CultureInfo.CurrentCulture ) + 1;
                                this._CountStatusBarPanel.Text = count.ToString( System.Globalization.CultureInfo.CurrentCulture );
                            }
                            while ( this._StartStopCheckBox.Checked & false );
                            break;
                        }

                    case Example.SlidingFFT:
                        {
                            if ( this._DoubleRadioButton.Checked )
                            {
                                this.SlidingFft( 0.5d );
                            }
                            else
                            {
                                this.SlidingFft( 0.5f );
                            }

                            break;
                        }

                    default:
                        {
                            break;
                        }
                }

                if ( this._StartStopCheckBox.Checked )
                {
                    this._StartStopCheckBox.Enabled = false;
                    this._StartStopCheckBox.Checked = false;
                    this._StartStopCheckBox.Enabled = true;
                }
            }
            else
            {
                this._CountStatusBarPanel.Text = "0";
            }
        }

        #endregion

    }
}
