using System;
using System.Windows.Forms;

namespace isr.Signals.Demo
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main( string[] args )
        {
            _ = Application.SetHighDpiMode( HighDpiMode.SystemAware );
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            CommandLineInfo.ParseCommandLine( args );
            Application.Run( new Console() );
        }
    }
}
