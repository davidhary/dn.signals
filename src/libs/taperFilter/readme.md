# About

isr.Signals.TaperFilter is a .Net library for digital signal filtering 
using Fourier Transforms and taper filters.

# How to Use

```
TBD
```

# Key Features

Provides base classes for:
* Taper Filter

# Main Types

The main types provided by this library are:

* _TaperFilter_ Taper filter.

# Feedback

isr.Signals.TaperFilter is released as open source under the MIT license. 
Bug reports and contributions are welcome at the [Signals Repository].

[Signals Repository]: https://bitbucket.org/davidhary/dn.signals

