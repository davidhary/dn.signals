# About

isr.Signals.Spectrum is a .Net library for computing
the power spectrum using Fourier Transforms.

# How to Use

```
TBD
```

# Key Features

Provides base classes for:
* Fourier transform

# Main Types

The main types provided by this library are:

* _Spectrum_ spectrum class.
* _PowerSpectrum_ spectrum class.

# Feedback

isr.Signals.Spectrum is released as open source under the MIT license. 
Bug reports and contributions are welcome at the [Signals Repository].

[Signals Repository]: https://bitbucket.org/davidhary/dn.signals
