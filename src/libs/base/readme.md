# About

isr.Signals.Base is a .Net library with base classes for the [Signals Repository].

# How to Use

```
TBD
```

# Key Features

Provides base classes for:
* Fourier transform

# Main Types

The main types provided by this library are:

* _FourierTransformBase_ base class for the Fourier Transform.

# Feedback

isr.Signals.Base is released as open source under the MIT license. 
Bug reports and contributions are welcome at the [Signals Repository].

[Signals Repository]: https://bitbucket.org/davidhary/dn.signals

