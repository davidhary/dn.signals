# About

isr.Signals.TaperWindow is a .Net library with classes for smoothing the Fourier transform using taper windows.

# How to Use

```
TBD
```

# Key Features

Provides base classes for:
* TaperWindow

# Main Types

The main types provided by this library are:

* _TaperWindow_ base class for a Taper Window.
* _BartlettTaperWindow_ Bartlett Taper Window.
* _BlackmanHarrisTaperWindow_ Blackman-Harris Taper Window.
* _BlackmanTaperWindow_ Blackman Taper Window.
* _HammingTaperWindow_ Hamming Taper Window.
* _HanningTaperWindow_ Hanning Taper Window.
* _SineTaperWindow_ Sine Taper Window.


# Feedback

isr.Signals.TaperWindow is released as open source under the MIT license. 
Bug reports and contributions are welcome at the [Signals Repository].

[Signals Repository]: https://bitbucket.org/davidhary/dn.signals
[FFTW Algorithm]: https://www.fftw.org
