using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace isr.Signals.TaperWindow
{
    /// <summary>   An array extensions. </summary>
    /// <remarks>   David, 2022-02-19. </remarks>
    internal static class ArrayExtensions
    {

        /// <summary>
        /// Multiplies the complex array real values by a scalar array and zeros the imaginary parts.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values">  The values. </param>
        /// <param name="scalars"> The scalars. </param>
        public static void ScaleReals( this Complex[] values, double[] scalars )
        {
            if ( values is object && values.Length > 0 && scalars is object && scalars.Length > 0 )
            {
                if ( values.GetLowerBound( 0 ) != scalars.GetLowerBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same lower bound" );
                }
                else if ( values.GetUpperBound( 0 ) != scalars.GetUpperBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same upper bound" );
                }
                else
                {
                    for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                        values[i] = new Complex( values[i].Real * scalars[i], 0d );
                }
            }
        }

        /// <summary> Multiplies the array elements by scalar elements. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values">  The values. </param>
        /// <param name="scalars"> The scalars. </param>
        public static void Scale( this double[] values, double[] scalars )
        {
            if ( values is object && values.Length > 0 && scalars is object && scalars.Length > 0 )
            {
                if ( values.GetLowerBound( 0 ) != scalars.GetLowerBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same lower bound" );
                }
                else if ( values.GetUpperBound( 0 ) != scalars.GetUpperBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same upper bound" );
                }
                else
                {
                    for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                        values[i] *= scalars[i];
                }
            }
        }

        /// <summary> Multiplies the array elements by scalar elements. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="values">  The values. </param>
        /// <param name="scalars"> The scalars. </param>
        public static void Scale( this float[] values, double[] scalars )
        {
            if ( values is object && values.Length > 0 && scalars is object && scalars.Length > 0 )
            {
                if ( values.GetLowerBound( 0 ) != scalars.GetLowerBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same lower bound" );
                }
                else if ( values.GetUpperBound( 0 ) != scalars.GetUpperBound( 0 ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( values ), values, "The arrays of values and scalars must have the same upper bound" );
                }
                else
                {
                    for ( int i = values.GetLowerBound( 0 ), loopTo = values.GetUpperBound( 0 ); i <= loopTo; i++ )
                        values[i] *= ( float ) scalars[i];
                }
            }
        }

    }
}
