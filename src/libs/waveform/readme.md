# About

isr.Signals.Waveform is a .Net library with classes for generating waveform signals.

# How to Use

```
TBD
```

# Key Features

Provides base classes for:
* Signal

# Main Types

The main types provided by this library are:

* _Frequencies_ Frequencies generator.
* _Times_ Times generator.
* _DC_ DC signal generator.
* _Ramp_ Ramp generator.
* _Random_ Random signal generator.
* _Sine_ Sine wave generator.

# Feedback

isr.Signals.Base is released as open source under the MIT license. 
Bug reports and contributions are welcome at the [Signals Repository].

[Signals Repository]: https://bitbucket.org/davidhary/dn.signals
[FFTW Algorithm]: https://www.fftw.org
