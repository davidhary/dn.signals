# About

isr.Signals.MeterNumerics is a .Net library with classes for computing 
the Mixed Radix Fourier Transform using the [Meta Numerics] algorithms.

# How to Use

```
TBD
```

# Key Features

Provides base classes for:
* Fourier transform

# Main Types

The main types provided by this library are:

* _MixedRadixFourierTransform_ Computer the Mixed Radix Fourier Transform.

# Feedback

isr.Signals.MetaNumerics is released as open source under the MIT license. 
Bug reports and contributions are welcome at the [Signals Repository].

[Signals Repository]: https://bitbucket.org/davidhary/dn.signals
[Meta Numerics]: http://www.meta-numerics.net

