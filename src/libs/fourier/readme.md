# About

isr.Signals.Foourier is a .Net library with classes 
to complete the sliding and discrete Fourier transforms.

# How to Use

```
TBD
```

# Key Features

Provides base classes for:
* Fourier transform

# Main Types

The main types provided by this library are:

* _DiscreteFourierTransform_ The Discrete Fourier Transform.
* _SlidingFourierTransform_ The Discrete Fourier Transform.

# Feedback

isr.Signals.Fourier is released as open source under the MIT license. 
Bug reports and contributions are welcome at the [Signals Repository].

[Signals Repository]: https://bitbucket.org/davidhary/dn.signals
