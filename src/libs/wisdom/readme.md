# About

isr.Signals.Wisdom is a .Net library for computing the fast Fourier transform using 
the [FFTW Algorithm].

# How to Use

```
TBD
```

# Key Features

* TBD

# Main Types

The main types provided by this library are:

* _TBD_ to be defined.

# Feedback

isr.Signals.Wisdom is released as open source under the MIT license. 
Bug reports and contributions are welcome at the [Signals Repository].

[Signals Repository]: https://bitbucket.org/davidhary/dn.signals
[FFTW Algorithm]: https://www.fftw.org
