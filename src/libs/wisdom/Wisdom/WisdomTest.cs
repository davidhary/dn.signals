using System;
using System.Diagnostics;

using isr.Signals.Wisdom;

namespace isr.Signals.Wisdom
{

    /// <summary> Tests the Wisdom FFT using managed methods. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public class WisdomTest : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public WisdomTest() : base()
        {
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets the dispose status sentinel. </summary>
        /// <value> The disposed. </value>
        protected bool Disposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        /// False if this method releases only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( this.Disposed ) return;
            try
            {
                if ( disposing )
                {
                    // Free managed resources when explicitly called
                }

                // Free shared unmanaged resources
                this._Cin?.Dispose();
                this._Cin = null;

                this._Cout?.Dispose();
                this._Cout = null;

                this._Fin = null;

                if ( this._ForwardEstimatePlan != IntPtr.Zero )
                {
                    Wisdom.FftwF.SafeNativeMethods.Destroy_plan( this._ForwardEstimatePlan );
                }

                if ( this._BackwardMeasurePlan != IntPtr.Zero )
                {
                    Wisdom.FftwF.SafeNativeMethods.Destroy_plan( this._BackwardMeasurePlan );
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.Disposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        ~WisdomTest()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        /// <summary>
        /// Mapped arrays
        /// </summary>
        private ComplexArrayF _Cin, _Cout;

        /// <summary>
        /// managed arrays
        /// </summary>
        private float[] _Fin;

        private IntPtr _ForwardEstimatePlan;

        private IntPtr _BackwardMeasurePlan;

        /// <summary> Initializes FFTW and all arrays. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="size"> logical size (number of elements) of the transform. </param>
        public void InitFftw( int size )
        {

            // create two managed arrays, possibly misaligned
            // n*2 because we are dealing with complex numbers
            this._Fin = new float[(size * 2)];
            // f out = New Single(n * 2 - 1) {}

            // fill the input array with a sawtooth signal
            for ( int i = 0, loopTo = size * 2 - 1; i <= loopTo; i += 2 )
                this._Fin[i] = i % 50;

            // create complex arrays
            var input = new ComplexArrayF( this._Fin, false );
            var output = new ComplexArrayF( this._Fin, false );

            // create a few test transforms
            this._ForwardEstimatePlan = Planner.Create( size, input, output, TransformDirection.Forward, PlannerOptions.Estimate ).Handle;
            this._BackwardMeasurePlan = Planner.Create( size, output, input, TransformDirection.Backward, PlannerOptions.Measure ).Handle;
        }

        /// <summary> Tests all plans. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public void TestAll()
        {
            TestPlan( this._ForwardEstimatePlan );
            TestPlan( this._BackwardMeasurePlan );
        }

        /// <summary> Tests a single plan, displaying results. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="plan"> Pointer to plan to test. </param>
        public static void TestPlan( IntPtr plan )
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            Wisdom.FftwF.SafeNativeMethods.Execute( plan );
            Console.WriteLine( "Time: {0} ms", stopwatch.Elapsed.TotalMilliseconds );

            // a: adds, b: m u l s, c: f m a s
            double a = 0d;
            double b = 0d;
            double c = 0d;
            Wisdom.FftwF.SafeNativeMethods.Flops( plan, ref a, ref b, ref c );
            Console.WriteLine( "Approx. flops: {0}", a + b + 2d * c );
        }
    }
}
