
using System;

namespace isr.Signals.Wisdom
{
    public partial class WisdomNativeTest
    {

        /// <summary>
        /// pointers to unmanaged arrays
        /// </summary>
        private IntPtr _Pin, _Pout;

        /// <summary>
        /// pointers to the FFTW plan objects
        /// </summary>
        private IntPtr _ForwardEstimatePlan, _ForwardPinnedEstimatePlan, _BackwardPinnedMeasurePlan;
    }
}
