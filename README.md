# Signals Libraries

Libraries for digital signal processing.

* [Binaries](#Binaries)
* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)
* [FFT implementations for .NET]  

<a name="Binaries"></a>
## Binaries

### FFTW Binaries
The Signals library expects the binaries, which are available at [Wisdom FFT Windows Binaries],
at a specific folder defined in the .csproj file:
```
C:\Apps\fftw\fftw-3.3.5-dll64
```

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories] are required:
* [Enums Libraries] - .NET Standard enums libraries
* [Std Libraries] - .NET Standard libraries
* [Charting Library] - Charting library
* [Signals Library] - Signals library
* [IDE Repository] - IDE support files.

```
git clone git@bitbucket.org:davidhary/dn.enums.git
git clone git@bitbucket.org:davidhary/dn.std.git
git clone git@bitbucket.org:davidhary/dn.signals.git
git clone git@bitbucket.org:davidhary/dn.charting.git
git clone git@bitbucket.org:davidhary/vs.ide.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
%dnlib%\core\enums
%dnlib%\core\std
%dnlib%\algorithms\signals
%dnlib%\visuals\charting
%vslib%\core\ide
```
where %dnlib% and %vslib% are  the root folders of the .NET libraries, e.g., %my%\lib\vs 
and %my%\libraries\vs, respectively, and %my% is the root folder of the .NET solutions

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository].

Restoring Editor Configuration:
```
xcopy /Y %my%\.editorconfig %my%\.editorconfig.bak
xcopy /Y %vslib%\core\ide\code\.editorconfig %my%\.editorconfig
```

Restoring Run Settings:
```
xcopy /Y %userprofile%\.runsettings %userprofile%\.runsettings.bak
xcopy /Y %vslib%\core\ide\code\.runsettings %userprofile%\.runsettings
```
where %userprofile% is the root user folder.

#### Packages
Presently, packages are consumed from a _source feed_ residing in a local folder, e.g., _%my%\nuget\packages_. 
The packages are 'packed', using the _Pack_ command from each packable project,
into the _%my%\nuget_ folder as specified in the project file and then
added to the source feed. Alternatively, the packages can be downloaded from the 
private [MEGA packages folder].

<a name="FacilitatedBy"></a>
## Facilitated By

* [Visual Studio](https://www.visualstudio.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - WiX Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [Code Converter](https://github.com/icsharpcode/CodeConverter) - Code Converter
* [Funduc Search and Replace](http://www.funduc.com/search_replace.htm) - Funduc Search and Replace for Windows

<a name="Repository-Owner"></a>
## Repository Owner
[ATE Coder]

<a name="Authors"></a>
## Authors
* [ATE Coder]  

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix] -- we are but a spec on the shoulders of giants  
* [John Simmons] - outlaw programmer  
* [Stack overflow] - Joel Spolsky  
* [.Net Foundation] - The .NET Foundation

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Enums Library]  
[Std Library]  
[Signals Library]  
[Charting Library]  
[Meta Numerics]  
[Wisdom FFT]  
[Wisdom FFT C\#]  

[MEGA packages folder]: https://mega.nz/folder/KEcVxC5a#GYnmvMcwP4yI4tsocD31Pg
[Enums Library]: https://bitbucket.org/davidhary/dn.enums
[Std Library]: https://bitbucket.org/davidhary/dn.std
[Signals Library]: https://bitbucket.org/davidhary/dn.signals
[Charting Library]: https://bitbucket.org/davidhary/dn.charting
[Meta Numerics]: http://www.meta-numerics.net
[Wisdom FFT]: http://www.fftw.org/
[Wisdom FFT Windows Binaries]: http://www.fftw.org/install/windows.html
[Wisdom FFT C\#]: http://www.sdss.jhu.edu/~tamas/bytes/fftwcsharp.html

[Microsoft /.NET Framework]: https://dotnet.microsoft.com/download

[external repositories]: ExternalReposCommits.csv
[IDE Repository]: https://www.bitbucket.org/davidhary/vs.ide

[ATE Coder]: https://www.IntegratedScientificResources.com
[Its all a remix]: https://www.everythingisaremix.info
[John Simmons]: https://www.codeproject.com/script/Membership/View.aspx?mid=7741
[Stack overflow]: https://www.stackoveflow.com

[Visual Studio]: https://www.visualstudio.com/
[Jarte RTF Editor]: https://www.jarte.com/ 
[WiX Toolset]: https://www.wixtoolset.org/
[Atomineer Code Documentation]: https://www.atomineerutils.com/
[EW Software Spell Checker]: https://github.com/EWSoftware/VSSpellChecker/wiki/
[Code Converter]: https://github.com/icsharpcode/CodeConverter
[Funduc Search and Replace]: http://www.funduc.com/search_replace.htm
[.Net Foundation]: https://source.dot.net

[FFT implementations for .NET]: https://www.codeproject.com/Articles/1095473/Comparison-of-FFT-implementations-for-NET


